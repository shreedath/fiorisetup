package com.hrms.portlet.controller;



import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.hrms.adduser.CreateUser;
import com.hrms.entity.AddressType;
import com.hrms.entity.CertificationDetails;
import com.hrms.entity.CertificationNameType;
import com.hrms.entity.CertificationType;
import com.hrms.entity.Country;
import com.hrms.entity.DegreeType;
import com.hrms.entity.Departmant;
import com.hrms.entity.Designation;
import com.hrms.entity.Document;
import com.hrms.entity.EmployementHistory;
import com.hrms.entity.Gender;
import com.hrms.entity.ManagerInfo;
import com.hrms.entity.ProjectDetails;
import com.hrms.entity.QualificationType;
import com.hrms.entity.QulificationNameType;
import com.hrms.entity.Relationship;
import com.hrms.entity.Role;
import com.hrms.entity.Skill;
import com.hrms.entity.SkillCategory;
import com.hrms.entity.SkillType;
import com.hrms.entity.State;
import com.hrms.entity.University;
import com.hrms.model.Address;
import com.hrms.model.AddressDetails;
import com.hrms.model.Certification;
import com.hrms.model.DocumentModel;
import com.hrms.model.EducationDetails;
import com.hrms.model.EducationDetailsModel;
import com.hrms.model.Employee;
import com.hrms.model.EmployeeProjectDetails;
import com.hrms.model.EmploymentDetails;
import com.hrms.model.FamilyDetails;
import com.hrms.model.FamilyModel;
import com.hrms.model.OtherDetails;
import com.hrms.model.ResetPassword;
import com.hrms.model.SkillDetails;
import com.hrms.model.Skills;
import com.hrms.model.UserRegistration;
import com.hrms.service.AddressService;
import com.hrms.service.CertificateDetailsService;
import com.hrms.service.DocumentService;
import com.hrms.service.EducationDetailsService;
import com.hrms.service.EmployeeProjectService;
import com.hrms.service.EmployeeService;
import com.hrms.service.EmployementHistoryService;
import com.hrms.service.FamilyDetailsService;
import com.hrms.service.SkillDetailsService;
import com.liferay.portal.kernel.dao.jdbc.OutputBlob;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.RoleServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

@Controller
@RequestMapping({"VIEW"})
public class UserRegistrationController
{
	
	
	
	 private static final String APPLICATION_PDF = "application/octet-stream";
	private final static String baseDir = "C:/upload";
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private SkillDetailsService skillService;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private EducationDetailsService educationDetailsService;
	
	
	@Autowired
	private FamilyDetailsService familyDetailsService;
	
	@Autowired
	private EmployementHistoryService employmentHistoryService;
	
	@Autowired
	private CertificateDetailsService certificateDetailsService;
	
	@Autowired
	private EmployeeProjectService employeeProjectService;
	 
	
    SimpleDateFormat dateFormat;
  
    Map< String, String > designationList = new HashMap<String, String>();
    Map< String, String > depertmentList = new HashMap<String, String>();
    Map< String, String > employeeManagerMap = new HashMap<String, String>();
    Map< String, String > genderMap = new HashMap<String, String>();
    Map< String, String > countryMap = new HashMap<String, String>();
    Map< String, String > stateMap = new HashMap<String, String>();
    Map< String, String > addressTypeMap = new HashMap<String, String>();
    Map< String, String > managerList = new HashMap<String, String>();
    Map<String,String> certificationTypeList=new HashMap<String, String>();
    Map<String,String> certificationList=new HashMap<String, String>();
    Map< String, String > roleMap = new HashMap<String, String>();
    Map< String, String > skillCategoryMap = new HashMap<String, String>();
    Map< String, String > skillMap = new HashMap<String, String>();
    Map< String, String > skillTypeMap = new HashMap<String, String>();
    Map< String, String > universityNameMap = new HashMap<String, String>();
    Map< String, String > qualificationTypeMap = new HashMap<String, String>();
    Map< String, String > qualificationMap = new HashMap<String, String>();
    //Map< String, String > genderMap = new HashMap<String, String>();
    Map< String, String > relationTypeMap = new HashMap<String, String>();
    
    Map< String, String > companyMap = new HashMap<String, String>();
    
    
    
    
    
    List<Designation> designationEntityList=null;
    List<Departmant> departmentEntityList=null;
    List<Country> countryEntityList=null;
    List<State> stateEntityList=null;
    List<AddressType> addressEntityList=null;
    List<ManagerInfo> managerEntityList=null;
    List<Address> address=null;
    List<String> versionList=new ArrayList<String>();
    List<String> proficiencyList=new ArrayList<String>();
    List<Role> roleEntityList =null;
    List<SkillCategory> skillEntityCategoryList=null;
    List<Skill> skillEntityList=null;
    List<SkillType> skillTypeEntityList=null;
    List<QualificationType> qualificationTypeList=null;
    List<QulificationNameType> qualificationNameTypeList=null;
    List<University> universityList=null;
    List<Gender> genderEntityList=null;
    
    Employee employee=null;
    AddressDetails addressDetails=null;
    String empId;
    
    
    private static final Log LOGGER = LogFactoryUtil.getLog(UserRegistrationController.class);
  
    @InitBinder
	public void initBinder(WebDataBinder binder) 
    {

	   dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	   dateFormat.setLenient(false);
	   binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	   
	   designationEntityList=employeeService.getDesignationList();
       departmentEntityList=employeeService.getDepartmentList();
       genderEntityList=employeeService.getGenderList();
       countryEntityList= employeeService.getCountryList();
       stateEntityList=employeeService.getStateList();
       addressEntityList=addressService.getAddressTypeList();
       managerEntityList=employeeService.getManagers();
       genderEntityList=employeeService.getGenderList();
        
       
       for(Gender gender :genderEntityList)
       {
       	genderMap.put(""+gender.getId(),gender.getName());
       }
       
       for(Designation designation :designationEntityList)
       {
       	designationList.put(designation.getDesiId().toString(),designation.getDesiName());
       }
       for(Departmant departmant :departmentEntityList)
       {
       	depertmentList.put(departmant.getDeptId().toString(),departmant.getDeptName());
       }
       for(Gender gender :genderEntityList)
       {
       	genderMap.put(""+gender.getId(),gender.getName());
       }
       
       for(Country country :countryEntityList)
       {
       	countryMap.put(""+country.getContryId(),country.getCountryName());
       }
       
       for(State state :stateEntityList)
       {
       	stateMap.put(""+state.getId()+"-"+state.getCountryId().getContryId(),state.getName());
       }
       for(AddressType addressType :addressEntityList)
       {
       	addressTypeMap.put(""+addressType.getId(),addressType.getName());
       }
       
       for(ManagerInfo managerType :managerEntityList)
       {
    	   managerList.put(managerType.getId()+ "-"+managerType.getDepartmentId().getDeptId(),managerType.getName());
       }
      

           if(versionList.size()==0){
           versionList.add("1.1");
           versionList.add("1.2");
           versionList.add("1.3");
           versionList.add("1.4");
           versionList.add("1.5");
           versionList.add("1.6");
           versionList.add("1.7"); 
           versionList.add("1.8");
           }
           if(proficiencyList.size()==0){
           proficiencyList.add("Beginner");
           proficiencyList.add("Intermediate");
           proficiencyList.add("Advanced ");
           proficiencyList.add("Expert");
           }
           

      
       /*proficiencyList.add("1");
       proficiencyList.add("2");
       proficiencyList.add("3");
       proficiencyList.add("4");
       proficiencyList.add("5");
       proficiencyList.add("6");*/
       
       roleEntityList =skillService.getRoles();
       skillEntityCategoryList=skillService.getSkillCategory();
       skillEntityList=skillService.getSkills();
       skillTypeEntityList=skillService.getSkillType();
       
       for(SkillCategory skillCategory:skillEntityCategoryList)
       {
    	   skillCategoryMap.put(""+skillCategory.getId(), skillCategory.getName());
       }
       
       /*********************************SKILL TYPE******************************/
       for(SkillType skillType:skillTypeEntityList)
       {
    	   skillTypeMap.put(""+skillType.getId(), skillType.getName());
       }
       
       /*********************************SKILL TYPE******************************/
       for(Skill skill:skillEntityList)
       {
    	   skillMap.put(""+skill.getId(), skill.getName());
       }
      
       /*********************************ROLE******************************/
       for(Role role:roleEntityList)
       {
    	   roleMap.put(""+role.getId(), role.getName());
       }
       
       
       
       qualificationTypeList=educationDetailsService.getQualificationType();
       universityList=educationDetailsService.getUniversity();
       qualificationNameTypeList=educationDetailsService.getQualificNameType();
       
       
       for(QualificationType qualificationType:qualificationTypeList)
       {
    	   qualificationTypeMap.put(""+qualificationType.getId(), qualificationType.getName());
       }

       for(University university:universityList)
       {
    	   universityNameMap.put(""+university.getId(), university.getName());
    	  
       }

       for(QulificationNameType qualiNameType:qualificationNameTypeList)
       {
    	   qualificationMap.put(""+qualiNameType.getId(), qualiNameType.getName());
       }
              
       relationTypeMap.put("1", "husband");
       relationTypeMap.put("2", "father");
       
        roleEntityList =skillService.getRoles(); 
        
        for(Role role:roleEntityList)
        {
     	   roleMap.put(""+role.getId(), role.getName());
        }
	}

	
  @RenderMapping
  public String defaultView(RenderRequest request,RenderResponse response,Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Default View");
    }
    
    String action=request.getParameter("render");
	if(null!=action)
	{
		if(action.equals("EmployeePersonalInfo"))
		{
			 Employee employee = new Employee();
			 request.setAttribute("employee", employee);
			 
			return "EmployeePersonalInfo";
		}
	}
	
	PortletSession session = request.getPortletSession();
	
	com.hrms.entity.Employee emp= new com.hrms.entity.Employee();
	Long userId = null;
	
	try {
		// get the current user 
		User currentUser = PortalUtil.getUser(request);
		
		userId = currentUser.getUserId();
		System.out.println(userId);
		// based on the current userID fetch respective employee
		emp = employeeService.getEmployeeByUserId(userId.toString());
		
		/*if(emp == null || !(emp.getStatus() == null || emp.getStatus().isEmpty()))
		{
			return "employeeDoesNotExist";	
		}
		else
		{*/
			// saving employee object in a session
			session.setAttribute("employe", emp, PortletSession.APPLICATION_SCOPE);
			
			System.out.println(emp);
		
			map.put("role", emp);
			
			return "UserView";
		//}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return action;
		return "";
	


  }
  
 // if the current user is HR display page to create new employee
	 @RenderMapping(params={"render=hrView"})
	 public String displayHrView(RenderRequest request,RenderResponse response,Map<String, Object> map)
	  {
	    if (LOGGER.isTraceEnabled()) {
	      LOGGER.trace("User Registration");
	    }
	   UserRegistration userReg=new UserRegistration();
	   map.put("userReg", userReg);
	    return "UserRegistration";
	  }
 
	 // if the current user is new Employee
	  @RenderMapping(params={"render=addDetails"})
	  public String addEmpdeatails(RenderRequest request,RenderResponse response,Map<String, Object> map)
	  {
		    if (LOGGER.isTraceEnabled()) {
		      LOGGER.trace("Add EmpPersonal Info");
		    }
		    
		    Long userId = null;
		    PortletSession session = request.getPortletSession();
			User currentUser;
			try {
				
				currentUser = PortalUtil.getUser(request);
				userId = currentUser.getUserId();
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			com.hrms.entity.Employee employee=null;
			if(null!=userId)
			{
				 employee = employeeService.getEmployeeByUserId(userId.toString());

			}
			else
			{
				employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);

				//employee=new com.hrms.entity.Employee();
			}

		    
		    //com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
		  //  com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);

		    System.out.println(employee);
		    Employee employeeModel = new Employee();
		    
		    if(null!=employee)
		    {
		    	employeeModel.setId(employee.getSid());
			    employeeModel.setEmployeeFirstName(employee.getfName());
			    employeeModel.setEmployeeMiddleName(employee.getmName());
			    employeeModel.setEmployeeLastName(employee.getlName());
			    employeeModel.setEmployeeDataOfBirth(employee.getDateOfBirth());
			    employeeModel.setEmployeeDateOfJoining(employee.getDateOfJoining());
			    employeeModel.setEmployeeEmailId(employee.getPersonalEmailId());
			    employeeModel.setEmployeeOfficialEmailId(employee.getOfficeEmailId());
			    employeeModel.setEmployeeId(employee.getEmpId());
			    employeeModel.setEmployeePhoneNo(employee.getMobileNo());
			    employeeModel.setEmployeeLandlineNo(employee.getHomeNo());
			    employeeModel.setEmployeePanNo(employee.getPanNo());
			    employeeModel.setEmployeeUanNo(employee.getUanNo());
			    employeeModel.setPassword(employee.getPassword());
			    employeeModel.setUserId(employee.getUserId());
				employeeModel.setGender(employee.getGender());
				employeeModel.setUserName(employee.getUserName());
				
				if(null!=employee.getDesignationId())
				{
					employeeModel.setDesignation(""+employee.getDesignationId().getDesiId());

				}
				if(null!=employee.getDepartmentId())
				{
		 			employeeModel.setDepertment(""+employee.getDepartmentId().getDeptId());

				}
	 			String mgrName = employee.getManagerId();
	 			System.out.println(mgrName);
	        	employeeModel.setManagerName(mgrName);
	        	
	        	if(null!=employee.getContryId())
	        	{
		        	employeeModel.setCountryId(""+employee.getContryId().getContryId());

	        	}
	        	if(null!=employee.getStateId())
	        	{
		        	employeeModel.setStateId(employee.getStateId().getId()+"-"+employee.getContryId().getContryId());

	        	}
	        	
			   // employeeModel.setManagerName(employee.getManagerId());
			    
		        employeeModel.setDesignationMap(designationList);
		        employeeModel.setDepartmentMap(depertmentList);
		        employeeModel.setEmployeeManagerMap(employeeManagerMap);
		        employeeModel.setGenderMap(genderMap);
		        employeeModel.setCountryMap(countryMap);
		        employeeModel.setStateMap(stateMap);
		        employeeModel.setManagerList(managerList);
			    		 
		    }
		   
		    
			 map.put("employeeModel", employeeModel);
			 
		    return "EmployeePersonalInfo";
	  }
	  
	  @RenderMapping(params={"render=resetPasswordPage"})
	  public String changePassword(RenderRequest request,RenderResponse response,Map<String, Object> map)
	  {
		    if (LOGGER.isTraceEnabled()) {
		      LOGGER.trace("View Pasword Rest");
		    }
		    ResetPassword rpass= new ResetPassword();
		    map.put("resetPassword",rpass);
		    return "ResetPassword";
	  }
	 
  @RenderMapping(params={"render=alternative-view"})
  public String alternativeView()
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Alternative view");
    }
    return "alternativeView";
  }
  
  @ActionMapping(params={"action=action-one"})
  public void actionOne()
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Action one");
    }
  }
  
  @ActionMapping(params={"action=action-two"})
  public void actionTwo(ActionResponse actionResponse)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Action two");
    }
    actionResponse.setRenderParameter("render", "alternative-view");
  }
  
  @ResourceMapping("resource-one")
  public void resourceOne(ResourceResponse resourceResponse)
    throws PortletException
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Resource one");
    }
    try
    {
      resourceResponse.setContentType("text/html");
      PrintWriter writer = resourceResponse.getWriter();
      writer.println("<p>This request handle the complete response. This is usefull to return JSON, images, files or any other resource that are needed by our portlets</p>");
    }
    catch (IOException e)
    {
      throw new PortletException(e);
    }
  }
  
  
  // create a new employee
  @ActionMapping("CreateEmployee")
  public void saveUserRegistration(@ModelAttribute("userReg") UserRegistration userReg,BindingResult result, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map) throws java.text.ParseException 
  {

	   if (LOGGER.isTraceEnabled()) {
		      LOGGER.trace("UserRegistration");
		    }
	   String newpassword = "";
		com.hrms.entity.Employee emp= new com.hrms.entity.Employee();
		
			Long userId = null;
			User newUser=null;
			Long newUserId = null;
	try {
				User currentUser = PortalUtil.getUser(actionRequest);
					
				userId = currentUser.getUserId();
				System.out.println(userId);

				CreateUser createUser = new CreateUser();
				UserRegistration userInfo = new UserRegistration();
				userInfo.setUserName(userReg.getUserName());
				userInfo.setFirstName(userReg.getFirstName());
				userInfo.setMiddleName(userReg.getMiddleName());
				userInfo.setLastName(userReg.getLastName());
				userInfo.setEmailId(userReg.getEmailId());
				
				
		//Create new user	
			 newUser = createUser.addUser(PortalUtil.getCompanyId(actionRequest), userInfo);
			 
				if(newUser == null)
				{
					System.out.println("Error in creation of new user");
				}
				else
					System.out.println("creation of new user is successfull");
		
		// create random password to update encrypted password in user_ table	
				Random random = new Random();
				for(int i=0;i<5;i++)
				{
					newpassword=newpassword+(char)(97 + random.nextInt(26));
				}
				
				newUserId = newUser.getUserId();
				com.liferay.portal.model.Role role= RoleLocalServiceUtil.getRole(CompanyLocalServiceUtil.getCompanyByMx("liferay.com").getCompanyId(), "Power User");
				
				//update new password at the user_ table in the lportal DB
				boolean flag = employeeService.updateLportalPassword(newpassword,newUserId);
				
				//insert created new user details to employee table
				emp.setUserName(userReg.getUserName());
				emp.setfName(newUser.getFirstName());
				emp.setmName(newUser.getMiddleName());
				emp.setlName(newUser.getLastName());
				emp.setPassword(newpassword);
				emp.setOfficeEmailId(newUser.getEmailAddress());
				emp.setEmpId(userReg.getEmployeeId());
				emp.setUserId(newUserId.toString());
				emp.setNoticePeriod(userReg.getNoticePeriod());
			    employeeService.saveEmployee(emp);
			    
			    // send mail to employee
			    boolean flag1 = employeeService.sendMailToEmp(emp);
			    if(flag1==true)
			    {
			    	System.out.println("mail sent successfully");
			    }
			    else
			    	System.out.println("Unable to send a mail");
			    
			    map.put("status",flag1);
				map.put("check",flag);
				actionResponse.setRenderParameter("render", "empCreationStatus-view");
				
		} catch (Exception e)
			{
				e.printStackTrace();
				map.put("check", newUser);
				actionResponse.setRenderParameter("render", "empCreationStatus-view");
			}
		
  }
  
  @RenderMapping(params={"render=empCreationStatus-view"})
  public String creationStatus()
  			{
	   			if (LOGGER.isTraceEnabled()) {
	   				LOGGER.trace("EmpCreationStatus");
	   			}
	   			return "EmpCreationStatus";
  			}
 
  @ActionMapping("passwordChange")
  public void resetPassword(@ModelAttribute("resetPassword") ResetPassword resetPassword,BindingResult result, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map) throws java.text.ParseException 
  {
	  String currentPassword;
	  Long userId = null;
	  boolean flag=false;
		try {
			User currentUser = PortalUtil.getUser(actionRequest);
			com.hrms.entity.Employee emp = new com.hrms.entity.Employee();	
			
			userId = currentUser.getUserId();
			
			emp = employeeService.getEmployeeByUserId(userId.toString());	
			currentPassword= emp.getPassword();
			System.out.println(currentPassword);
			
			String oldpassword = resetPassword.getOldPassword();
			String newpassword = resetPassword.getNewPassword();
			
			 flag = employeeService.resetLportalPassword(oldpassword, newpassword, currentPassword, userId);	
			 
			 if(flag==true)
			 {
				 emp.setPassword(newpassword);
				 employeeService.saveEmployee(emp);
			 }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		map.put("flag",flag);
		actionResponse.setRenderParameter("render", "resetStatus-view");
  }
  
  @RenderMapping(params={"render=resetStatus-view"})
  public String resetStatus()
  			{
	   			if (LOGGER.isTraceEnabled()) {
	   				LOGGER.trace("Password reset status");
	   			}
	   			return "ResetStatus";
  			}
  
  @ActionMapping(params={"action=personalInfo-one"})
  public void testRenderMethod(Map<String, Object> map, ActionRequest actionRequest, ActionResponse actionResponse)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("testRenderMethod");
    }
   
    try
    {
	        
        address = new ArrayList<Address>();
        addressDetails = new AddressDetails();
          
    	
		UserRegistration userReg=(UserRegistration) PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("userReg");
  
      if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee") != null)
      {
			        employee = (Employee)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee");
			        
			        
	  }
      else
      {
    	  	User currentUser = PortalUtil.getUser(actionRequest);
    	  	System.out.println(currentUser.getRoles());
			currentUser.getUserId();
			com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 

			
    	  			//employeeService.getEmployeeByUserId("10199"); 
			        employee = new Employee();
			        address = new ArrayList<Address>();
			        addressDetails = new AddressDetails();
        
			        
			        if(null!=userReg)
			        {
			        	employee.setEmployeeId(userReg.getEmployeeId());
			        	employee.setEmployeeFirstName(userReg.getFirstName());
			        	employee.setEmployeeMiddleName(userReg.getMiddleName());
			        	employee.setEmployeeLastName(userReg.getLastName());
			        	employee.setManagerName("Shreedath");
			        }
			        if(null!=employeeEntity)
			        {
			        	empId=""+employeeEntity.getSid();
			        	employee.setId(employeeEntity.getSid());
			        	employee.setEmployeeId(employeeEntity.getEmpId());
			        	employee.setEmployeeFirstName(employeeEntity.getfName());
			        	employee.setEmployeeMiddleName(employeeEntity.getmName());
			        	employee.setEmployeeLastName(employeeEntity.getlName());
			        	employee.setGender(employeeEntity.getGender());
			        	employee.setEmployeeDataOfBirth(employeeEntity.getDateOfBirth());
			        	employee.setEmployeeDateOfJoining(employeeEntity.getDateOfJoining());
			        	employee.setDesignation(""+employeeEntity.getDesignationId().getDesiId());
			        	employee.setEmployeePhoneNo(employeeEntity.getMobileNo());
			        	employee.setEmployeeLandlineNo(employeeEntity.getHomeNo());
			        	employee.setEmployeeEmailId(employeeEntity.getPersonalEmailId());
			        	employee.setEmployeeOfficialEmailId(employeeEntity.getOfficeEmailId());
			        	employee.setDepertment(""+employeeEntity.getDepartmentId().getDeptId());
			        	employee.setManagerName(employeeEntity.getManagerId());
			        }
			       
			        System.out.println("***************************else******************");
     
      }
      
				      employee.setDesignationMap(designationList);
				      employee.setDepartmentMap(depertmentList);
				      employee.setEmployeeManagerMap(employeeManagerMap);
				      employee.setGenderMap(genderMap);
				      employee.setCountryMap(countryMap);
				      employee.setStateMap(stateMap);
				      employee.setManagerList(managerList);
				      
				      addressDetails.setAddess(address);
				      addressDetails.setCountryMap(countryMap);
				      addressDetails.setStateMap(stateMap);
				      addressDetails.setAddressTypeMap(addressTypeMap);
	      
	      
				      map.put("employee", employee);
				      map.put("address", addressDetails);
	      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
   
    				actionResponse.setRenderParameter("render", "personalInfo-view");
  }
  
  @RenderMapping(params={"render=personalInfo-view"})
  public String personalInfoView(RenderRequest actionRequest, Map<String, Object> map, Model model)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("personalInfoView");
    }
    try
    {
    	
      if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee") != null)
      {
			        Employee employee = (Employee)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee");
			        
			        
			        address = new ArrayList<Address>();
			        addressDetails = new AddressDetails();
			        
			        User currentUser = PortalUtil.getUser(actionRequest);
			        List<com.liferay.portal.model.Role> roles=currentUser.getRoles();
					 System.out.println(roles);
					 
					currentUser.getUserId();
					com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
			       	
			        employee.setDesignationMap(designationList);
			        employee.setDepartmentMap(depertmentList);
			        employee.setEmployeeManagerMap(employeeManagerMap);
			        employee.setGenderMap(genderMap);
			        employee.setCountryMap(countryMap);
			        employee.setStateMap(stateMap);
			        employee.setManagerList(managerList);
			        
			        map.put("employee", employee);
			        
			        addressDetails.setAddess(address);
			        addressDetails.setCountryMap(countryMap);
			        addressDetails.setStateMap(stateMap);
			        addressDetails.setAddressTypeMap(addressTypeMap);
			        map.put("address", addressDetails);
					UserRegistration userReg=(UserRegistration) PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("userReg");

			        if(null!=userReg)
			        {
			        	employee.setEmployeeId(userReg.getEmployeeId());
			        	employee.setEmployeeFirstName(userReg.getFirstName());
			        	employee.setEmployeeMiddleName(userReg.getMiddleName());
			        	employee.setEmployeeLastName(userReg.getLastName());
			        	
			        }
					   
			        
			        if(null!=employeeEntity)
			        {
			        	employee.setId(employeeEntity.getSid());
			        	employee.setEmployeeId(employeeEntity.getEmpId());
			        	employee.setEmployeeFirstName(employeeEntity.getfName());
			        	employee.setEmployeeMiddleName(employeeEntity.getmName());
			        	employee.setEmployeeLastName(employeeEntity.getlName());
			        	employee.setGender(employeeEntity.getGender());
			        	employee.setEmployeeDataOfBirth(employeeEntity.getDateOfBirth());
			        	employee.setEmployeeDateOfJoining(employeeEntity.getDateOfJoining());
			        	employee.setDesignation(""+employeeEntity.getDesignationId().getDesiId());
			        	employee.setEmployeePhoneNo(employeeEntity.getMobileNo());
			        	employee.setEmployeeLandlineNo(employeeEntity.getHomeNo());
			        	employee.setEmployeeEmailId(employeeEntity.getPersonalEmailId());
			        	employee.setEmployeeOfficialEmailId(employeeEntity.getOfficeEmailId());
			         	employee.setDepertment(""+employeeEntity.getDepartmentId().getDeptId());
			        	employee.setManagerName(employeeEntity.getManagerId());
			        }
	  }
      else
      {
    	  
    	  	employee = new Employee();
    	  	address = new ArrayList<Address>();
		    addressDetails = new AddressDetails();
		    
			UserRegistration userReg=(UserRegistration) PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("userReg");
 
			User currentUser = PortalUtil.getUser(actionRequest);
			
			 List<com.liferay.portal.model.Role> roles=RoleServiceUtil.getUserRoles(currentUser.getUserId());
			 System.out.println(roles);
			 
			com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
	       	
	        employee.setDesignationMap(designationList);
	        employee.setDepartmentMap(depertmentList);
	        employee.setEmployeeManagerMap(employeeManagerMap);
	        employee.setGenderMap(genderMap);
	        employee.setCountryMap(countryMap);
	        employee.setStateMap(stateMap);
	        employee.setManagerList(managerList);
	        
	        map.put("employee", employee);
	        
	        addressDetails.setAddess(address);
	        addressDetails.setCountryMap(countryMap);
	        addressDetails.setStateMap(stateMap);
	        addressDetails.setAddressTypeMap(addressTypeMap);
	        map.put("address", addressDetails);

	        if(null!=userReg)
	        {
	        	employee.setEmployeeId(userReg.getEmployeeId());
	        	employee.setEmployeeFirstName(userReg.getFirstName());
	        	employee.setEmployeeMiddleName(userReg.getMiddleName());
	        	employee.setEmployeeLastName(userReg.getLastName());
	        	
	        }
			   
	        
	        if(null!=employeeEntity)
	        {
	        	employee.setId(employeeEntity.getSid());
	        	employee.setEmployeeId(employeeEntity.getEmpId());
	        	employee.setEmployeeFirstName(employeeEntity.getfName());
	        	employee.setEmployeeMiddleName(employeeEntity.getmName());
	        	employee.setEmployeeLastName(employeeEntity.getlName());
	        	employee.setGender(employeeEntity.getGender());
	        	employee.setEmployeeDataOfBirth(employeeEntity.getDateOfBirth());
	        	employee.setEmployeeDateOfJoining(employeeEntity.getDateOfJoining());
	        	employee.setDesignation(""+employeeEntity.getDesignationId().getDesiId());
	        	employee.setEmployeePhoneNo(employeeEntity.getMobileNo());
	        	employee.setEmployeeLandlineNo(employeeEntity.getHomeNo());
	        	employee.setEmployeeEmailId(employeeEntity.getPersonalEmailId());
	        	employee.setEmployeeOfficialEmailId(employeeEntity.getOfficeEmailId());
	        	employee.setDepertment(""+employeeEntity.getDepartmentId().getDeptId());
	        	employee.setManagerName(employeeEntity.getManagerId());
	        }
	        System.out.println("***************************else******************");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "EmployeePersonalInfo";
  }
  
  @ActionMapping("savePersonalInfo")
  public void savePersonalInfo(@ModelAttribute("employeeModel") Employee employeeModel,BindingResult result, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map) throws java.text.ParseException
  {
		    if (LOGGER.isTraceEnabled()) {
		      LOGGER.trace("***************************savePersonalInfo*****************************");
		    }
		    
		   // PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("employe", employeeModel);
		    PortletSession session = actionRequest.getPortletSession();
		    
		    com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
		    
		    com.hrms.entity.Employee employeeEntity=new com.hrms.entity.Employee();
		    
			// saveAndExit
			if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
			 {
				
				if(!employeeModel.getManagerName().equalsIgnoreCase(""))
				{
					employeeEntity.setManagerId(employeeModel.getManagerName());
				}
			    
			    //employeeEntity.setSid(employeeModel.getId());
			    employeeEntity.setSid(employee.getSid());
			    employeeEntity.setPassword(employee.getPassword());
			    employeeEntity.setUserId(employee.getUserId());
			    employeeEntity.setUserName(employee.getUserName());
			    
			    if(!employeeModel.getDesignation().equalsIgnoreCase(""))
			    {
		    Designation designation= new Designation();
		    designation.setDesiId(Long.valueOf(employeeModel.getDesignation()));
		    employeeEntity.setDesignationId(designation);
		    
		    designation.setDesiName(employeeModel.getDesignation());
	 	    
		    
		    for(Designation des:designationEntityList)
		    {
		    	if(des.getDesiId().toString().equals(designation.getDesiName()))
		    	{
		    		employeeEntity.setDesignation(des.getDesiName());
				    	}
				    }
		    	}
			    employeeEntity.setEmpId(employeeModel.getEmployeeId());
			    employeeEntity.setfName(employeeModel.getEmployeeFirstName());
			    employeeEntity.setmName(employeeModel.getEmployeeMiddleName());
			    employeeEntity.setlName(employeeModel.getEmployeeLastName());
			    		    
			    employeeEntity.setDateOfBirth(employeeModel.getEmployeeDataOfBirth());
			    employeeEntity.setDateOfJoining(employeeModel.getEmployeeDateOfJoining());
			    
			    if(!employeeModel.getDepertment().equalsIgnoreCase(""))
			    {
				    Departmant department=new Departmant();
				    department.setDeptId(Long.valueOf(employeeModel.getDepertment()));
				    employeeEntity.setDepartmentId(department);
		    }
			    employeeEntity.setMobileNo(employeeModel.getEmployeePhoneNo());
			    employeeEntity.setPersonalEmailId(employeeModel.getEmployeeEmailId());
			   
			    if(!employeeModel.getGender().equalsIgnoreCase(""))
			    {
			    	 employeeEntity.setGender(employeeModel.getGender());
			    }
			   
			    employeeEntity.setOfficeEmailId(employeeModel.getEmployeeOfficialEmailId());
			    employeeEntity.setHomeNo(employeeModel.getEmployeeLandlineNo());
			    
			    if(!(employeeModel.getCountryId()).equalsIgnoreCase(""))
			    {
				    Country country = new Country();
				    country.setContryId(Long.valueOf(employeeModel.getCountryId()));
				    employeeEntity.setContryId(country);
			    }

			    if(!employeeModel.getStateId().equalsIgnoreCase(""))
			    {
				    State state = new State();
				    String str = employeeModel.getStateId().split("-")[0];
				    state.setId(Long.valueOf(str));
		    //employeeEntity.setDateOfBirth(dateFormat.parse(employee.getEmployeeDataOfBirth()));
				    employeeEntity.setStateId(state);
			    }

			    employeeService.saveEmployee(employeeEntity);
					
			    actionResponse.setRenderParameter("render", "saveExitSuccess-view"); 
			 }
			else
			{
			    employeeEntity.setManagerId(employeeModel.getManagerName());
			    //employeeEntity.setSid(employeeModel.getId());
			    employeeEntity.setSid(employee.getSid());
			    employeeEntity.setPassword(employee.getPassword());
			    employeeEntity.setUserId(employee.getUserId());
			    employeeEntity.setUserName(employee.getUserName());
			    
			    Designation designation= new Designation();
			    designation.setDesiId(Long.valueOf(employeeModel.getDesignation()));
			    employeeEntity.setDesignationId(designation);
			    
			    designation.setDesiName(employeeModel.getDesignation());
		 	    
			    
			    for(Designation des:designationEntityList)
			    {
			    	if(des.getDesiId().toString().equals(designation.getDesiName()))
			    	{
			    		employeeEntity.setDesignation(des.getDesiName());
			    	}
			    }		  

		    employeeEntity.setEmpId(employeeModel.getEmployeeId());
		    employeeEntity.setfName(employeeModel.getEmployeeFirstName());
		    employeeEntity.setmName(employeeModel.getEmployeeMiddleName());
		    employeeEntity.setlName(employeeModel.getEmployeeLastName());
		    		    
		    employeeEntity.setDateOfBirth(employeeModel.getEmployeeDataOfBirth());
		    employeeEntity.setDateOfJoining(employeeModel.getEmployeeDateOfJoining());
		    
		    Departmant department=new Departmant();
		    department.setDeptId(Long.valueOf(employeeModel.getDepertment()));
		    employeeEntity.setDepartmentId(department);
		    
		    employeeEntity.setMobileNo(employeeModel.getEmployeePhoneNo());
		    employeeEntity.setPersonalEmailId(employeeModel.getEmployeeEmailId());
		    //employeeEntity.setPanNo(employee.getEmployeePanNo());
		    //employeeEntity.setp
		    //employeeEntity.setUanNo(employee.getEmployeeUanNo());
		   
		    employeeEntity.setGender(employeeModel.getGender());
		    employeeEntity.setOfficeEmailId(employeeModel.getEmployeeOfficialEmailId());
		    employeeEntity.setHomeNo(employeeModel.getEmployeeLandlineNo());
		    
		    Country country = new Country();
		    country.setContryId(Long.valueOf(employeeModel.getCountryId()));
		    employeeEntity.setContryId(country);
		    
		    State state = new State();
		    String str = employeeModel.getStateId().split("-")[0];
		    state.setId(Long.valueOf(str));
		    //state.setId(Long.valueOf(employeeModel.getStateId()));
		    employeeEntity.setStateId(state);
		    
		    System.out.println(employeeService);
		    employeeService.saveEmployee(employeeEntity);
		    
		    
		    actionResponse.setRenderParameter("render", "addressRender-view");
			}
  }
  
  
  @ActionMapping("addRecord")
  public void saveAndExit(@ModelAttribute("employee") Employee employee, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("***************************saveAndExit*****************************");
    }
    PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("employee", employee);
    actionResponse.setRenderParameter("render", "addressRender-view");
  }
  
  
  @ActionMapping("handleCustomer")
  public void getCustomerData(@ModelAttribute("employee") Employee employee, ActionRequest actionRequest, ActionResponse actionResponse, Model model)
  {
    actionResponse.setRenderParameter("action", "success");
    
    model.addAttribute("successModel", employee);
  }
  
  @RenderMapping(params={"action=success"})
  public String viewSuccess()
  {
    LOGGER.info("#############Calling viewSuccess###########");
    
    return "success";
  }
  
  @ActionMapping(params={"action=employeeAddress"})
  public void addressAction(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    System.out.println("***************************addressAction***************************");
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("addressAction");
    }
    try
    {
    	
    	User currentUser = PortalUtil.getUser(actionRequest);
 	  	System.out.println(currentUser.getRoles());
		currentUser.getUserId();
		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId());
		List<com.hrms.entity.Address> addressEntityList=null;
		if(null!=employeeEntity)
		{
			addressEntityList=addressService.getAddressByEmpId(""+employeeEntity.getSid());

		}
    	
    	 AddressDetails addressDetails =null;
    	
    	
      if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("address") != null)
      {
         addressDetails = (AddressDetails)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("address");
        
        
       
      }
      else
      {
        addressDetails = new AddressDetails();
        List<Address> addresslist = new ArrayList<Address>();

        if(null!=addressEntityList && addressEntityList.size()>0)
        {
        	for(com.hrms.entity.Address address:addressEntityList)
        	{
        		Address add=new Address();
        		add.setSlNo(""+address.getId());
        		add.setCity(address.getCity());
        		add.setLineone(address.getLine1());
        		add.setLinetwo(address.getLine2());
        		add.setLinethree(address.getLine3());
        		add.setPincode(""+address.getPincode());
        		
        		if(null!=address.getCountry())
        		{
        			add.setCountry(""+address.getCountry().getContryId());
        			
        		}
        		if(null!=address.getState())
        		{
        			if(null!=address.getCountry())
            		{
            			//add.setCountry(""+address.getCountry().getContryId());
            			add.setState(address.getState().getId()+"-"+address.getCountry().getContryId());	
            			
            		}
        			
        		}
        		
	        	if(null!=address.getAddressTypeId())
	        	{
	        		add.setAddresstype(""+address.getAddressTypeId().getId());  
	        	}      
	        	
	        	add.setEnable(address.isEnable());
	        	
	        	if(null!=address.getEffectiveDate() && !address.getEffectiveDate().equals(""))
	        	{
	        		add.setEffectiveDate(address.getEffectiveDate());
	        	}
	        	if(null!=address.getTillDate() && !address.getTillDate().equals(""))
	        	{
	        		add.setTillDate(address.getTillDate());
	        	}
	        	
	        	add.setId(""+address.getId());
        		empId=""+address.getEmployeeId().getSid();
        		addresslist.add(add);
        		
        	}
        	
        	
        }
        /*else
    	{
    		Address add=new Address();
    		addresslist.add(add);
    	}*/
        addressDetails.setAddess(addresslist);
        
        System.out.println("***************************else******************");
      }
      
      
      addressDetails.setCountryMap(countryMap);
      addressDetails.setStateMap(stateMap);
      addressDetails.setAddressTypeMap(addressTypeMap);
      map.put("addressDetails", addressDetails);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    actionResponse.setRenderParameter("render", "addressRender-view");
  }
  
  @RenderMapping(params={"render=addressRender-view"})
  public String addressRenderView(RenderRequest request, Map<String, Object> map, Model model)
  {
	    try
	    {
	    	
	    	User currentUser = PortalUtil.getUser(request);
		  	System.out.println(currentUser.getRoles());
			currentUser.getUserId();
			com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId());
			List<com.hrms.entity.Address> addressEntityList=null;
			if(null!=employeeEntity)
			{
				addressEntityList=addressService.getAddressByEmpId(""+employeeEntity.getSid());

			}
	    	
	    	 AddressDetails addressDetails =null;
	    	
	    	
	     /* if (PortalUtil.getHttpServletRequest(request).getSession().getAttribute("address") != null)
	      {
	         addressDetails = (AddressDetails)PortalUtil.getHttpServletRequest(request).getSession().getAttribute("address");
	        
	        
	       
	      }*/
	      //else
	      //{
	        addressDetails = new AddressDetails();
	        List<Address> addresslist = new ArrayList<Address>();

	        if(null!=addressEntityList && addressEntityList.size()>0)
	        {
	        	for(com.hrms.entity.Address address:addressEntityList)
	        	{
	        		Address add=new Address();
	        		add.setSlNo(""+address.getId());
	        		add.setCity(address.getCity());
	        		add.setLineone(address.getLine1());
	        		add.setLinetwo(address.getLine2());
	        		add.setLinethree(address.getLine3());
	        		add.setPincode(""+address.getPincode());
	        		
	        		if(null!=address.getState())
	        		{
	        			if(null!=address.getCountry())
	            		{
	            			//add.setCountry(""+address.getCountry().getContryId());
	            			add.setState(address.getState().getId()+"-"+address.getCountry().getContryId());	
	            			
	            		}
	        			
	        		}
	        		if(null!=address.getCountry())
	        		{
	        			add.setCountry(""+address.getCountry().getContryId());
	        		}
		        	if(null!=address.getAddressTypeId())
		        	{
		        		add.setAddresstype(""+address.getAddressTypeId().getId());  
		        	}
	        		   
		        	if(null!=address.getEffectiveDate() && !address.getEffectiveDate().equals(""))
		        	{
		        		add.setEffectiveDate(address.getEffectiveDate());
		        	}
		        	if(null!=address.getTillDate() && !address.getTillDate().equals(""))
		        	{
		        		add.setTillDate(address.getTillDate());
		        	}
		        	
	        		add.setEnable(address.isEnable());
	        		add.setId(""+address.getId());
	        		
	        		empId=""+address.getEmployeeId().getSid();
	        		addresslist.add(add);
	        		
	        	}
	        	
	        	
	        	
	        }
	        
		          addressDetails.setAddess(addresslist);
		          System.out.println("***************************else******************");
	      //}
	      
	      
			      addressDetails.setCountryMap(countryMap);
			      addressDetails.setStateMap(stateMap);
			      addressDetails.setAddressTypeMap(addressTypeMap);
			      map.put("addressDetails", addressDetails);
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
    
	    return "EmployeeAddress";
  }
  
  @RenderMapping(params={"render=saveExitSuccess-view"})
  public String saveExitSuccess()
  			{
	   			if (LOGGER.isTraceEnabled()) {
	   				LOGGER.trace("saveExitSuccess");
	   			}
	   			return "saveAndExitSuccess";
  			}
  
  @ActionMapping("saveAddressInfo")
  //@ActionMapping(params={"action=saveAddressInfo"})
  public void saveAddressInfo(@ModelAttribute("add") Address address, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("***************************saveAddressInfo*****************************");
    }
    
   
    
    String inputString = actionRequest.getParameter("addressDetails");
    JSONParser parser = new JSONParser();
    List<Address> addressList = new ArrayList<Address>();
    List<com.hrms.entity.Address> addressEntityList=new ArrayList<com.hrms.entity.Address>();
    try
    {
    	  User currentUser = PortalUtil.getUser(actionRequest);
    	  currentUser.getUserId();
    	  com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
    	  JSONArray json = (JSONArray)parser.parse(inputString);
      
    	  
    	 // saveAndExit
    	  if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
    	  {
    		  System.out.println("save and exit");
    		      		  
      		  for (int i = 0; i < json.size(); i++)
    	      {
    	        
    	        com.hrms.entity.Address addressEntity=new com.hrms.entity.Address();
    	        
    	        JSONObject objects = (JSONObject)json.get(i);
    	              
    	        //com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
    	        //emp.setSid(employeeEntity.getSid());
    	        //addressEntity.setEmployeeId(emp);
    	        addressEntity.setEmployeeId(employeeEntity);
    	        
    	        addressEntity.setLine1((String)objects.get("addressLine1"));
    	        addressEntity.setLine2((String)objects.get("addressLine2"));
    	        addressEntity.setLine3((String)objects.get("addressLine3"));
    	        addressEntity.setCity((String)objects.get("city"));

    	        String pinCode =(String)objects.get("pincode");
    	        if(!pinCode.equalsIgnoreCase(""))
    	        {
    	    	       addressEntity.setPincode(Long.parseLong(pinCode));
    	        }
    	        System.out.println(pinCode);

    	    
    	       // addressEntity.setPincode(new Long((String)objects.get("pincode")));
    	            	        
    	        com.hrms.entity.AddressType addressType=new  com.hrms.entity.AddressType();
    	        if(null!=(String)objects.get("addressType") && !((String)objects.get("addressType")).equals("0"))
				{
    	        	addressType.setId(new Long((String)objects.get("addressType")));
					addressEntity.setAddressTypeId(addressType);
    	        }

    	        com.hrms.entity.Country countyEntity=new Country();
    	        if(null!=(String)objects.get("country") && !((String)objects.get("country")).equals("0"))
				{
				String countryId=(String)objects.get("country");
    	        countyEntity.setContryId(new Long(countryId));
    	        addressEntity.setCountry(countyEntity);
    	        }
				
    	        com.hrms.entity.State stateEntity=new com.hrms.entity.State();				
    	        if(null!=(String)objects.get("state") && !((String)objects.get("state")).equals("0"))
				{				
    	        String stateId=(String)objects.get("state");
    	        String []stateList=stateId.split("-");
    	        stateId=stateList[0];
    	        stateEntity.setId(new Long(stateId));
    	        addressEntity.setState(stateEntity);
    	        }

    	        addressEntity.setEnable(true);
    	        
    	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	  	          {
    	        	addressEntity.setId(Long.valueOf((String)objects.get("slNo")));

	  	          }
    	        addressEntity.setEffectiveDate((String)objects.get("effectiveDate"));
		    	addressEntity.setTillDate((String)objects.get("tillDate"));
		    	
    	        addressEntityList.add(addressEntity);
    	        
    	        Address address1=new Address();
    	        address1.setLineone((String)objects.get("addressLine1"));
    	        address1.setLinetwo((String)objects.get("addressLine2"));
    	        address1.setLinethree((String)objects.get("addressLine3"));
    	        address1.setCity((String)objects.get("city"));
    	        address1.setCountry((String)objects.get("country"));
    	        address1.setState((String)objects.get("state"));
    	        address1.setPincode((String)objects.get("pincode"));
    	        
    	        addressEntityList.add(addressEntity);
    	        
    	        addressList.add(address1);
    	      }  
      		  
      		 addressService.saveAddressDetailList(addressEntityList);
      		 
    		 actionResponse.setRenderParameter("render", "saveExitSuccess-view"); 
    	  }
    	  else
    	  {
    		  System.out.println("save"); 
    		  
    		  for (int i = 0; i < json.size(); i++)
    	      {
    	        //Address address1 = new Address();
    	        com.hrms.entity.Address addressEntity=new com.hrms.entity.Address();
    	        
    	        JSONObject objects = (JSONObject)json.get(i);
    	              
    	        String countryId=(String)objects.get("country");
    	        addressEntity.setLine1((String)objects.get("addressLine1"));
    	        addressEntity.setLine2((String)objects.get("addressLine2"));
    	        addressEntity.setLine3((String)objects.get("addressLine3"));
    	        addressEntity.setCity((String)objects.get("city"));
    	        
    	        com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
    	        emp.setSid(employeeEntity.getSid());
    	        addressEntity.setEmployeeId(emp);
    	        
    	        com.hrms.entity.AddressType addressType=new  com.hrms.entity.AddressType();
    	        if(null!=(String)objects.get("addressType") && !((String)objects.get("addressType")).equals("0")){
    	        	
    	        }
    	        addressType.setId(new Long((String)objects.get("addressType")));
    	        addressEntity.setAddressTypeId(addressType);
    	        com.hrms.entity.Country countyEntity=new Country();
    	        countyEntity.setContryId(new Long(countryId));
    	        addressEntity.setCountry(countyEntity);
    	        com.hrms.entity.State stateEntity=new com.hrms.entity.State();
    	        String stateId=(String)objects.get("state");
    	        String []stateList=stateId.split("-");
    	        stateId=stateList[0];
    	        stateEntity.setId(new Long(stateId));
    	        addressEntity.setState(stateEntity);
    	        
    	        //addressEntity.setPincode(new Long((String)objects.get("pincode")));
    	        addressEntity.setEnable(true);
    	        addressEntityList.add(addressEntity);
    	        
    	        Address address1=new Address();
    	        address1.setLineone((String)objects.get("addressLine1"));
    	        address1.setLinetwo((String)objects.get("addressLine2"));
    	        address1.setLinethree((String)objects.get("addressLine3"));
    	        address1.setCity((String)objects.get("city"));
    	        address1.setCountry((String)objects.get("country"));
    	        address1.setState((String)objects.get("state"));
    	       // address1.setPincode((String)objects.get("pincode"));
    	        String pinCode =(String)objects.get("pincode");
    	        if(!pinCode.equalsIgnoreCase(""))
    	        {
    	    	       addressEntity.setPincode(Long.parseLong(pinCode));
    	        }
    	        System.out.println(pinCode);
    	        
    	        
    	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	  	          {
  	        	addressEntity.setId(Long.valueOf((String)objects.get("slNo")));

	  	          }
    	       
    	        
		    	addressEntity.setEffectiveDate((String)objects.get("effectiveDate"));
		    	addressEntity.setTillDate((String)objects.get("tillDate"));
		    	
		    	
    	        addressEntityList.add(addressEntity);
    	        
    	        addressList.add(address1);
    	      }
    	      addressService.saveAddressDetailList(addressEntityList);
    	      if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee") != null)
    	      {
    		        Employee employee = (Employee)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee");
    		        System.out.println(employee.getEmployeeId());
    		      //addressEntity.
    	      }
    	      
    	      AddressDetails addressDetails = new AddressDetails();
      	    addressDetails.setAddess(addressList);
      	    PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("address", addressDetails);
      	    
      	    actionResponse.setRenderParameter("render", "Skills-view");
      	  
    	  }
    	  
    	  
     
    }
    catch (ParseException e)
    {
      e.printStackTrace();
      actionResponse.setRenderParameter("render", "failed-view");
    } catch (PortalException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		actionResponse.setRenderParameter("render", "failed-view");
	} catch (SystemException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		actionResponse.setRenderParameter("render", "failed-view");
	}
    
  }
  @ActionMapping("saveAddressInfo1")
  //@ActionMapping(params={"action=saveAddressInfo1"})
  public void saveAddressInfo1(@ModelAttribute("add") Address address, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("***************************saveAddressInfo1*****************************");
    }
 
    
    actionResponse.setRenderParameter("render", "Skills-view");
  }
  @ActionMapping(params={"action=employeeDetails"})
  public void educationDetailsAction(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("employeeDetailsAction");
    }
    try
    {
    	
    	
    	EducationDetailsModel educationDetailsModels=null;
    	
      if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("educationDetailsModels") != null)
      {
	        educationDetailsModels = (EducationDetailsModel)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("educationDetailsModels");
	        
	       
	        
	   	 	
	   	 	educationDetailsModels.setQualificationMap(qualificationMap);
	   	 	educationDetailsModels.setQualificationTypeMap(qualificationTypeMap);
	   	 	educationDetailsModels.setUniversityNameMap(universityNameMap);
   	 	
	   	 	map.put("educationDetailsModels", educationDetailsModels);
	   	 	
      }
      else
      {
         
    	  	educationDetailsModels = new EducationDetailsModel();
	        //List<EducationDetails> educationList = new ArrayList();
	        //educationDetailsModels.setEducationDetailList(educationList);
	       
	   	 	
	   	 	educationDetailsModels.setQualificationMap(qualificationMap);
	   	 	educationDetailsModels.setQualificationTypeMap(qualificationTypeMap);
	   	 	educationDetailsModels.setUniversityNameMap(universityNameMap);
	   	 	map.put("educationDetailsModels", educationDetailsModels);
	   	 	System.out.println("***************************else******************");
	   	 	
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    actionResponse.setRenderParameter("render", "employeeDetailsRender-view");
  }
  
  @RenderMapping(params={"render=employeeDetailsRender-view"})
  public String educationDetailsRenderView(RenderRequest request, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("employeeDetailsRenderView");
    }
    try
    {
		    	User currentUser = PortalUtil.getUser(request);
			  	System.out.println(currentUser.getRoles());
				currentUser.getUserId();
		 		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId());
				
				List<com.hrms.entity.EducationDetails> educationDetailList=educationDetailsService.getEducationDetailsByEmpId(""+employeeEntity.getSid());
				
				List<EducationDetails> educationDetailListVal=new ArrayList<EducationDetails>();
				
    			EducationDetailsModel educationDetailsModels=null;
		      if (PortalUtil.getHttpServletRequest(request).getSession().getAttribute("educationDetailsModels") != null)
		      {
		       
		    	    educationDetailsModels = (EducationDetailsModel)PortalUtil.getHttpServletRequest(request).getSession().getAttribute("educationDetailsModels");
		   	 	    
		    	    
		    	    if(null!=educationDetailList && educationDetailList.size()>0)
		    	    {
		    	    	for(com.hrms.entity.EducationDetails educationDetails:educationDetailList)
		    	    	{
		    	    		
		    	    		EducationDetails educationDetails2=new EducationDetails();
		    	    		
		    	    		Employee employee=new Employee();
		    	    		employee.setId(educationDetails.getEmpId().getSid());
		    	    		educationDetails2.setSid(""+educationDetails.getEduId());
		    	    		educationDetails2.setEnable(educationDetails.isEnable());
		    	    		if(null!=educationDetails.getQualificationId() && !educationDetails.getQualificationId().equals("0"))
		    	    		{
		    	    			educationDetails2.setQualificationType(""+educationDetails.getQualificationId().getId());
		    	    		}
		    	    		
		    	    		//educationDetails2.setCollegeName(educationDetails.getCollegeName());
		    	    		if(null!=educationDetails.getDegreeId())
			    	    		educationDetails2.setQualification(""+educationDetails.getDegreeId().getId());
		    	    		
		    	    		if(null!=educationDetails.getUniversityId() && !educationDetails.getUniversityId().equals("0"))
		    	    		{
			    	    		educationDetails2.setUniversityName(""+educationDetails.getUniversityId().getId());
		    	    		}
		    	    		
		    	    		educationDetails2.setJoinDate(educationDetails.getJoinDate());
		    	    		educationDetails2.setEndDate(educationDetails.getEndDate());
		    	    		educationDetails2.setPercentage(educationDetails.getPercentage());
		    	    		educationDetails2.setCollegeName(educationDetails.getCollegeName());
		    	    		educationDetails2.setGrade(educationDetails.getGrade());
		    	    		educationDetails2.setSpecilization(educationDetails.getSpecilization());//now hard coaded remove later
		    	    		
		    	    		
		    	    		educationDetailListVal.add(educationDetails2);
		    	    		empId=""+educationDetails.getEduId();
		    	    	}
		    	    }
		    	    
			   	 	
		      }
		      else
		      {
		    	  educationDetailsModels = new EducationDetailsModel();
		   	 	
		    	  	if(null!=educationDetailList && educationDetailList.size()>0)
		    	    {
		    	    	for(com.hrms.entity.EducationDetails educationDetails:educationDetailList)
		    	    	{
		    	    		
		    	    		Employee employee=new Employee();
		    	    		employee.setId(educationDetails.getEmpId().getSid());
		    	    		EducationDetails educationDetails2=new EducationDetails();
		    	    		educationDetails2.setSid(""+educationDetails.getEduId());
		    	    		educationDetails2.setEnable(educationDetails.isEnable());
		    	    		if(null!=educationDetails.getQualificationId())
		    	    		educationDetails2.setQualificationType(""+educationDetails.getQualificationId().getId());
		    	    		if(null!=educationDetails.getDegreeId())
		    	    		educationDetails2.setQualification(""+educationDetails.getDegreeId().getId());
		    	    		educationDetails2.setCollegeName(educationDetails.getCollegeName());
		    	    		if(null!=educationDetails.getUniversityId())
		    	    		educationDetails2.setUniversityName(""+educationDetails.getUniversityId().getId());
		    	    		educationDetails2.setJoinDate(educationDetails.getJoinDate());
		    	    		educationDetails2.setEndDate(educationDetails.getEndDate());
		    	    		educationDetails2.setPercentage(educationDetails.getPercentage());
		    	    		educationDetails2.setCollegeName(educationDetails.getCollegeName());
		    	    		educationDetails2.setGrade(educationDetails.getGrade());
		    	    		 educationDetails2.setSpecilization(educationDetails.getSpecilization());
		    	    		educationDetailListVal.add(educationDetails2);
		    	    		empId=""+educationDetails.getEduId();
		    	    	}
		    	    }
		         
		        System.out.println("***************************else******************");
		        
		        
		        
		      }
		      
		      	educationDetailsModels.setEducationDetailList(educationDetailListVal);
		      	educationDetailsModels.setQualificationMap(qualificationMap);
		   	 	educationDetailsModels.setQualificationTypeMap(qualificationTypeMap);
		   	 	educationDetailsModels.setUniversityNameMap(universityNameMap);
		        map.put("educationDetailsModels", educationDetailsModels);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "EducationDetails";
  }
  
  @ActionMapping("eduDetails")
  public void saveEducationDetails(@ModelAttribute("educationDetailsModel") EducationDetailsModel educationDetailsModel, ActionResponse actionResponse, ActionRequest actionRequest) throws PortalException, SystemException
  {
    System.out.println("***************************************************************");
    System.out.println(actionRequest);
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("employeeDetailsAction");
    }
    
    
    User currentUser = PortalUtil.getUser(actionRequest);
	  currentUser.getUserId();
	  com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
	  
	  
    String inputString = actionRequest.getParameter("EducationDetails");
    JSONParser parser = new JSONParser();
    List<EducationDetails> educationList = new ArrayList<EducationDetails>();
    List<com.hrms.entity.EducationDetails> educationDetailEntityList=new ArrayList<com.hrms.entity.EducationDetails>();
    
    
    
    try
    {
      JSONArray json = (JSONArray)parser.parse(inputString);
      
      
   // saveAndExit
	  if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
	  {
		  System.out.println("save and exit");
		  for (int i = 0; i < json.size(); i++)
	      {
	    	com.hrms.entity.EducationDetails educationDetails=new com.hrms.entity.EducationDetails();
	        //EducationDetails education = new EducationDetails();
	        JSONObject objects = (JSONObject)json.get(i);
	       
	        DegreeType degreetype=new DegreeType();
	        if(null!=objects.get("qualification")&& !objects.get("qualification").equals(""))
	        {
	        	 Long qualificationId=new Long((String)objects.get("qualification"));
	 	        
	 	        degreetype.setId(qualificationId);
	 	        educationDetails.setDegreeId(degreetype);
	        }
	       
	        
	        com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
	        emp.setSid(employeeEntity.getSid());
	       // emp.setSid(3l);
	        educationDetails.setEmpId(emp);
	        
	        educationDetails.setCollegeName((String)objects.get("college"));
	        educationDetails.setEndDate((String)objects.get("endDate"));
	        educationDetails.setJoinDate((String)objects.get("startDate"));
	        
	        educationDetails.setPercentage((String)objects.get("percentage"));
	        
	        QualificationType qualificationType=new QualificationType();
	        if(null!=(String)objects.get("qualificationType") && !((String)objects.get("qualificationType")).equals(""))
	        {
	        	   Long qualificationTypeId=new Long((String)objects.get("qualificationType"));
	   	        	qualificationType.setId(qualificationTypeId);
	   	        	educationDetails.setQualificationId(qualificationType);
	   	        
	        }
	     
	        
	        University universityId=new University();
	        Long uniId=new Long((String)objects.get("board"));
	        universityId.setId(uniId);
	        educationDetails.setUniversityId(universityId);
	        
	        educationDetails.setGrade((String)objects.get("grade"));
	        educationDetails.setEnable(true);
	        educationDetails.setSpecilization((String)objects.get("specilization"));
	        
	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	          {
	        	educationDetails.setEduId(Long.valueOf((String)objects.get("slNo")));

	          }
	        educationDetailEntityList.add(educationDetails);
	       
	      }
	      educationDetailsService.saveEducationDetailsList(educationDetailEntityList);
		  actionResponse.setRenderParameter("render", "saveExitSuccess-view");
	  }
	  else
	  {
		  for (int i = 0; i < json.size(); i++)
	      {
	    	com.hrms.entity.EducationDetails educationDetails=new com.hrms.entity.EducationDetails();
	        //EducationDetails education = new EducationDetails();
	        JSONObject objects = (JSONObject)json.get(i);
	       
	        DegreeType degreetype=new DegreeType();
	        Long qualificationId=new Long((String)objects.get("qualification"));
	        degreetype.setId(qualificationId);
	        educationDetails.setDegreeId(degreetype);
	        
	        com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
	        emp.setSid(employeeEntity.getSid());
	        emp.setSid(3l);
	        educationDetails.setEmpId(emp);
	        
	        educationDetails.setCollegeName((String)objects.get("college"));
	        educationDetails.setEndDate((String)objects.get("endDate"));
	        educationDetails.setJoinDate((String)objects.get("startDate"));
	        
	        educationDetails.setPercentage((String)objects.get("percentage"));
	        
	        QualificationType qualificationType=new QualificationType();
	        Long qualificationTypeId=new Long((String)objects.get("qualificationType"));
	        qualificationType.setId(qualificationTypeId);
	        educationDetails.setQualificationId(qualificationType);
	        
	        
	        University universityId=new University();
	        Long uniId=new Long((String)objects.get("board"));
	        universityId.setId(uniId);
	        educationDetails.setUniversityId(universityId);
	        
	        educationDetails.setGrade((String)objects.get("grade"));
	        educationDetails.setEnable(true);
	        educationDetails.setSpecilization((String)objects.get("specilization"));
	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	          {
	        	educationDetails.setEduId(Long.valueOf((String)objects.get("slNo")));

	          }
	        educationDetailEntityList.add(educationDetails);
	        //educationDetails.setQualificationId(qualificationId);
	        //educationDetails.setDegreeId(degreeId);
	        
	        
	        //education.setsNo(i);
	        //education.setCollegeName((String)objects.get("college"));
	        //education.setEduId((String)objects.get("qualification"));
	       // education.setEndDate((String)objects.get("endDate"));
	        //education.setJoinDate((String)objects.get("startDate"));
	       // education.setPercentage((String)objects.get("percentage"));
	       // education.setUniversityName((String)objects.get("board"));
	        //education.setSpecilization((String)objects.get("specialization"));
	        //educationList.add(education);
	      }
	      educationDetailsService.saveEducationDetailsList(educationDetailEntityList);
	      
	      EducationDetailsModel educationDetailsModels = new EducationDetailsModel();
	      educationDetailsModels.setEducationDetailList(educationList);
	      PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("educationDetailsModels", educationDetailsModels);
	      actionResponse.setRenderParameter("render", "familyDetails-view");

	  }
      
      
    }
    catch (ParseException e)
    {
      e.printStackTrace();
      actionResponse.setRenderParameter("render", "failed-view");
    }
  }
  
  @ActionMapping(params={"action=skills"})
  public void skillsAction(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("testRenderMethod");
    }
    try
    {
    	
    	User currentUser = PortalUtil.getUser(actionRequest);
	  	System.out.println(currentUser.getRoles());
		currentUser.getUserId();
		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
		List<com.hrms.entity.SkillDetails> skills=null;
		if(null!=employeeEntity)
		{
			skills=skillService.getSkillByEmpId(""+employeeEntity.getSid());
		}
		
		
		Skills employeeSkills=null;
        
      if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employeeSkills") != null)
      {
        
    	   employeeSkills = (Skills)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("skillDetails");
        
           
           /****************************************Version******************************/
	       map.put("versionList", versionList);
	        
	       /*******************************Proficiency************************* */  
	   
	        map.put("proficiencyList", proficiencyList);
        
           /*********************************SKILL CATEGORY******************************/
                 
	       // employeeSkills.setSkillCategoryMap(skillCategoryMap);
	       // employeeSkills.setSkillTypeMap(skillTypeMap);
	        //employeeSkills.setSkillMap(skillMap);
	       // employeeSkills.setRoleMap(roleMap);
            map.put("employeeSkills", employeeSkills);
      }
      else
      {
    	  
    	  employeeSkills=new Skills();
    	     
    	  SkillDetails skillDetails = new SkillDetails();
          
          List<Skills> skillList = new ArrayList<Skills>();
          
          int i=0;
          
    	   // employeeSkills = new Skills();
    	  
    	    if(null!=skills && skills.size()>0)
    	    {
    	    	for(com.hrms.entity.SkillDetails skillDetail:skills)
    	    	{
    	    		Skills skill=new Skills();
    	    		
    	    		//skillDetail.getSlNo()
    	    		//skillDetail.getEmpId().getSid()
    	    		skill.setSlNO(""+skillDetail.getSlNo());
    	    		skill.setProficiency(skillDetail.getProficiency());
    	    		skill.setRelevantExpInMonths(skillDetail.getRelevantExpInMonths());
    	    		skill.setRelevantExpInYears(skillDetail.getRelevantExpInYears());
    	    		
    	    		if(null!=skillDetail.getSkillCategoryId())
    	    		{
        	    		skill.setSkillCategory(""+skillDetail.getSkillCategoryId().getId());

    	    		}
    	    		if(null!=skillDetail.getSkillTypeId())
    	    		{
        	    		skill.setSkillType(""+skillDetail.getSkillTypeId().getId());

    	    		}
    	    		if(null!=skillDetail.getSkillId())
    	    		{
        	    		skill.setSkill(""+skillDetail.getSkillId().getId());

    	    		}
    	    		if(null!=skillDetail.getRoleId())
    	    		{
        	    		skill.setRole(""+skillDetail.getRoleId().getId());

    	    		}
    	    		skill.setVersion(skillDetail.getVersion());
    	    		skill.setLastUsed(skillDetail.getLastUsed());
    	    		skill.setEnable(skillDetail.isEnable());
    	    		skillList.add(skill);
    	    	}
    	    }
    	    
    	    skillDetails.setSkills(skillList);
    	   
    	    map.put("skillDetails", skillDetails);
             System.out.println("***************************else******************");
      }
      
        /****************************************Version******************************/
	       map.put("versionList", versionList);
	        
	    /*******************************Proficiency************************* */  
	       map.put("proficiencyList", proficiencyList);
      
        /*********************************SKILL CATEGORY******************************/
        
	       
	    employeeSkills.setSkillCategoryMap(skillCategoryMap);
        employeeSkills.setSkillTypeMap(skillTypeMap);
        employeeSkills.setSkillMap(skillMap);
        employeeSkills.setRoleMap(roleMap);
        map.put("employeeSkills", employeeSkills);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    actionResponse.setRenderParameter("render", "Skills-view");
  }
  
  @RenderMapping(params={"render=Skills-view"})
  public String SkillsView(RenderRequest request, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("SkillsView");
    }
    try
    {
    	
    	User currentUser = PortalUtil.getUser(request);
	  	System.out.println(currentUser.getRoles());
		currentUser.getUserId();
		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
		List<com.hrms.entity.SkillDetails> skills=null;
		if(null!=employeeEntity)
		{
			 skills=skillService.getSkillByEmpId(""+employeeEntity.getSid());

		}
		
		Skills employeeSkills=null;
        
      if (PortalUtil.getHttpServletRequest(request).getSession().getAttribute("employeeSkills") != null)
      {
        
    	   employeeSkills = (Skills)PortalUtil.getHttpServletRequest(request).getSession().getAttribute("skillDetails");
        
           
           /****************************************Version******************************/
	       map.put("versionList", versionList);
	        
	       /*******************************Proficiency************************* */  
	   
	        map.put("proficiencyList", proficiencyList);
        
           /*********************************SKILL CATEGORY******************************/
                 
	       // employeeSkills.setSkillCategoryMap(skillCategoryMap);
	       // employeeSkills.setSkillTypeMap(skillTypeMap);
	        //employeeSkills.setSkillMap(skillMap);
	       // employeeSkills.setRoleMap(roleMap);
            map.put("employeeSkills", employeeSkills);
      }
      else
      {
    	  employeeSkills=new Skills();
    	     
    	  SkillDetails skillDetails = new SkillDetails();
          
          List<Skills> skillList = new ArrayList<Skills>();
          
          
          
    	   // employeeSkills = new Skills();
          int i=0;
    	    if(null!=skills && skills.size()>0)
    	    {
    	    	for(com.hrms.entity.SkillDetails skillDetail:skills)
    	    	{
    	    		Skills skill=new Skills();
    	    		skill.setSlNO(""+skillDetail.getSlNo());
    	    		//skillDetail.getSlNo()
    	    		//skillDetail.getEmpId().getSid()
    	    		skill.setProficiency(skillDetail.getProficiency());
    	    		skill.setRelevantExpInMonths(skillDetail.getRelevantExpInMonths());
    	    		skill.setRelevantExpInYears(skillDetail.getRelevantExpInYears());
    	    		
    	    		if(null!=skillDetail.getSkillCategoryId())
    	    		{
        	    		skill.setSkillCategory(""+skillDetail.getSkillCategoryId().getId());

    	    		}
    	    		if(null!=skillDetail.getSkillTypeId())
    	    		{
        	    		skill.setSkillType(""+skillDetail.getSkillTypeId().getId());

    	    		}
    	    		if(null!=skillDetail.getSkillId())
    	    		{
        	    		skill.setSkill(""+skillDetail.getSkillId().getId());

    	    		}
    	    		if(null!=skillDetail.getRoleId())
    	    		{
        	    		skill.setRole(""+skillDetail.getRoleId().getId());

    	    		}
    	    		skill.setVersion(skillDetail.getVersion());
    	    		skill.setLastUsed(skillDetail.getLastUsed());
    	    		skill.setEnable(skillDetail.isEnable());
    	    		skillList.add(skill);
    	    	}
    	    }
    	    
    	    skillDetails.setSkills(skillList);
    	   
    	    map.put("skillDetails", skillDetails);
             System.out.println("***************************else******************");
      }
      
        /****************************************Version******************************/
	       map.put("versionList", versionList);
	        
	    /*******************************Proficiency************************* */  
	       map.put("proficiencyList", proficiencyList);
      
        /*********************************SKILL CATEGORY******************************/
              
	    employeeSkills.setSkillCategoryMap(skillCategoryMap);
        employeeSkills.setSkillTypeMap(skillTypeMap);
        employeeSkills.setSkillMap(skillMap);
        employeeSkills.setRoleMap(roleMap);
        map.put("employeeSkills", employeeSkills);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "EmployeeSkills";
  }
  
  @ActionMapping("saveSkillInfo")
  public void saveSkillInfo(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("***************************saveSkillInfo*****************************");
    }
    
    
    //GETTING FROM THE REQUEST PARAMETER
    String inputString = actionRequest.getParameter("skillDetails");
    JSONParser parser = new JSONParser();
    
    //ENTITY MODEL LIST
    List<com.hrms.entity.SkillDetails> skilldetailsList=new ArrayList<com.hrms.entity.SkillDetails>();
    //MODEL LIST
    List<Skills> skillList = new ArrayList<Skills>();
    try
    {
    	
    	User currentUser = PortalUtil.getUser(actionRequest);
    	currentUser.getUserId();
    	com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
    	
      JSONArray json = (JSONArray)parser.parse(inputString);
      
   // saveAndExit
	  if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
	  {
		  System.out.println("save and exit");
		  for (int i = 0; i < json.size(); i++)
	      {
	    	 
	    	//-------------------------------------------MODEL ------------------------------------------
	        Skills skills = new Skills();
	        JSONObject objects = (JSONObject)json.get(i);
	        skills.setVersion((String)objects.get("version"));
	        skills.setSkillCategory((String)objects.get("skillCategory"));
	        skills.setProficiency((String)objects.get("proficiency"));
	        //skills.setLastUsed((Date)objects.get("lastUsed"));
	        //skills.setRelevantExp((Double)objects.get("relevantExperience"));
	        skills.setSkillType((String)objects.get("skillType"));
	        skills.setSkill((String)objects.get("skills"));
	        skills.setRole((String)objects.get("role"));
	        skillList.add(skills);
	        
	        //----------------------------------------Adding Entity skill details-------------------------
	        com.hrms.entity.SkillDetails skillDetails=new com.hrms.entity.SkillDetails();
	        skillDetails.setVersion((String)objects.get("version"));
	        
	        SkillCategory skillCategory=new SkillCategory();
	        String catId=(String) objects.get("skillCategory");
	        if(null!=catId  && !catId.equals("") )
	        {
	        	skillCategory.setId(new Long(catId));
		        skillDetails.setSkillCategoryId(skillCategory);
	        }
	        
	        
	        skillDetails.setProficiency((String)objects.get("proficiency"));
	       
	        skillDetails.setLastUsed((String)objects.get("lastUsed"));
	        skillDetails.setRelevantExpInMonths((String)objects.get("relevantExperienceMonths"));
	         skillDetails.setRelevantExpInYears((String)objects.get("relevantExperienceYears"));
	        
	        SkillType skillType=new  SkillType();
	        String skillTypeId=(String)objects.get("skillType");
	        
	        if(null!=skillTypeId  && !skillTypeId.equals("") )
	        {
	        	 skillType.setId(new Long(skillTypeId));
	 	        skillDetails.setSkillTypeId(skillType);
	        }
	        
	        
	       
	        Skill skill=new Skill();
	        String skillId=(String)objects.get("skills");
	        
	        if(null!=skillId  && !skillId.equals("") )
	        {
	        	skill.setId(new Long(skillId));
		        skillDetails.setSkillId(skill);
	        }
	        
	        
	        Role role=new Role();
	        String roleId=(String)objects.get("role");
	        if(null!=roleId  && !roleId.equals("") )
	        {
	        	role.setId(new Long(roleId));
		        skillDetails.setRoleId(role);
	        }
	        
	        
	        com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
	        emp.setSid(employeeEntity.getSid());
	        skillDetails.setEnable(true);
	        skillDetails.setEmpId(emp);
	        
	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	          {
	        	skillDetails.setSlNo(Long.valueOf((String)objects.get("slNo")));

	          }
	        
	        skilldetailsList.add(skillDetails);
	        
	      }
	      //---------------------------------Saving skill details----------------------------
	      skillService.saveSkillDetailList(skilldetailsList);
		  actionResponse.setRenderParameter("render", "saveExitSuccess-view");
	  }
	  else
	  {
		  for (int i = 0; i < json.size(); i++)
	      {
	    	 
	    	//-------------------------------------------MODEL ------------------------------------------
	        Skills skills = new Skills();
	        JSONObject objects = (JSONObject)json.get(i);
	        skills.setVersion((String)objects.get("version"));
	        skills.setSkillCategory((String)objects.get("skillCategory"));
	        skills.setProficiency((String)objects.get("proficiency"));
	        //skills.setLastUsed((Date)objects.get("lastUsed"));
	        //skills.setRelevantExp((Double)objects.get("relevantExperience"));
	        skills.setSkillType((String)objects.get("skillType"));
	        skills.setSkill((String)objects.get("skills"));
	        skills.setRole((String)objects.get("role"));
	        skillList.add(skills);
	        
	        //----------------------------------------Adding Entity skill details-------------------------
	        com.hrms.entity.SkillDetails skillDetails=new com.hrms.entity.SkillDetails();
	        skillDetails.setVersion((String)objects.get("version"));
	        
	        SkillCategory skillCategory=new SkillCategory();
	        String catId=(String) objects.get("skillCategory");
	        skillCategory.setId(new Long(catId));
	        skillDetails.setSkillCategoryId(skillCategory);
	        
	        skillDetails.setProficiency((String)objects.get("proficiency"));
	       
	        /*SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	        String dateInString = (String) objects.get("lastUsed");

	        try {

	            Date date = formatter.parse(dateInString);
	            System.out.println(date);
	            System.out.println(formatter.format(date));

	        }  catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        */
	        
	        skillDetails.setLastUsed((String)objects.get("lastUsed"));
	        skillDetails.setRelevantExpInMonths((String)objects.get("relevantExperienceMonths"));
	         skillDetails.setRelevantExpInYears((String)objects.get("relevantExperienceYears"));
	        
	        SkillType skillType=new  SkillType();
	        String skillTypeId=(String)objects.get("skillType");
	        skillType.setId(new Long(skillTypeId));
	        skillDetails.setSkillTypeId(skillType);
	        Skill skill=new Skill();
	        String skillId=(String)objects.get("skills");
	        skill.setId(new Long(skillId));
	        skillDetails.setSkillId(skill);
	        Role role=new Role();
	        String roleId=(String)objects.get("role");
	        role.setId(new Long(roleId));
	        skillDetails.setRoleId(role);
	        
	        com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
	        emp.setSid(employeeEntity.getSid());
	       // emp.setEmployeeId(emp);
	        //com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
	       // emp.setSid(3l);
	        skillDetails.setEnable(true);
	        skillDetails.setEmpId(emp);
	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	          {
	        	skillDetails.setSlNo(Long.valueOf((String)objects.get("slNo")));

	          }
	        skilldetailsList.add(skillDetails);
	        
	      }
	      //---------------------------------Saving skill details----------------------------
	      skillService.saveSkillDetailList(skilldetailsList);
	      //ADDING INTO THE PARENT LIST
	      SkillDetails skillDetails = new SkillDetails();
	      skillDetails.setSkills(skillList);
	      //ADDING INTO THE SESSION
	      PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("skillDetails", skillDetails);
	      actionResponse.setRenderParameter("render", "employeeDetailsRender-view");
	  }
      
      
    }
    catch (ParseException e)
    {
      e.printStackTrace();
      actionResponse.setRenderParameter("render", "failed-view");
    } catch (PortalException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		actionResponse.setRenderParameter("render", "failed-view");
	} catch (SystemException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		actionResponse.setRenderParameter("render", "failed-view");
	}
    
  }
  
  @ActionMapping(params={"action=employmentDetails"})
  public void employmentDetailsAction(ActionResponse actionResponse ,Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("testRenderMethod");
    }
    
     EmploymentDetails employmentDetails=new EmploymentDetails();
     
     employmentDetails.setDesignationMap(designationList);
     employmentDetails.setRoleMap(roleMap);
     map.put("employmentDetails", employmentDetails);
     
     actionResponse.setRenderParameter("render", "employmentDetails-view");
  }
  
  @RenderMapping(params={"render=employmentDetails-view"})
  public String employmentDetailsView(RenderRequest actionRequest,Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("employmentDetails");
    }

    List<EmployementHistory> employementHistorieList=null;
    EmploymentDetails employmentDetails=new EmploymentDetails();
    
    try {
    	
    	User currentUser=null;
    	currentUser = PortalUtil.getUser(actionRequest);
		System.out.println(currentUser.getRoles());
		
		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId());
		if(null!=employeeEntity)
		{
			employementHistorieList=employmentHistoryService.getEmploymentHistoryByCompanyEmployee(""+employeeEntity.getSid());	
		}
		
		  List<EmploymentDetails> employmentDetailList = new ArrayList<EmploymentDetails>();
	      
	        if(null!=employementHistorieList && employementHistorieList.size()>0)
	        {
	        	for(EmployementHistory emphistory:employementHistorieList)
	        	{
	        		EmploymentDetails empModel=new EmploymentDetails();
	        		
	        		empModel.setCompanyName(emphistory.getCompany());
	        		if(null!=emphistory.getDesignationId())
	        		empModel.setDesignation(""+emphistory.getDesignationId().getDesiId());
	        		empModel.setEndDate(emphistory.getEndDate());
	        		empModel.setStartDate(emphistory.getStartDate());
	        		if(null!=emphistory.getRoleId())
	        		empModel.setRole(""+emphistory.getRoleId().getId());
	        		empModel.setEnable(emphistory.isEnable());
	        		empModel.setId(""+emphistory.getId());
	        		employmentDetailList.add(empModel);
	        		
	        	}
	        	employmentDetails.setEmploymentDetailsList(employmentDetailList);
	        }	
		
    }
    catch(Exception e)
    {
    	e.printStackTrace();
    }
    
    
    
   
   
     employmentDetails.setDesignationMap(designationList);
     employmentDetails.setRoleMap(roleMap);
     map.put("employmentDetails", employmentDetails);
    
    return "employmentDetails";
  }
  
  @ActionMapping("saveEmploymentInfo")
  public void saveEmploymentDetailsInfo(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
     if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("***************************saveEmploymentDetailsInfo*****************************");
    }
     String inputString = actionRequest.getParameter("EmploymentDetails");
    JSONParser parser = new JSONParser();
    List<EmployementHistory> employmentHistoryList=new ArrayList<EmployementHistory>();
   // List<ProjectDetails> projectDetailList=new  ArrayList<ProjectDetails>();
    try
    {
    	
    	User currentUser = PortalUtil.getUser(actionRequest);
    	currentUser.getUserId();
    	com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
    	
    	
    	
      JSONArray json = (JSONArray)parser.parse(inputString);
      //JSONObject objectInArray = (JSONObject)json.get(0);
      //System.out.println(objectInArray);
      
      // saveAndExit
   	  if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
   	  {
   		  System.out.println("save and exit");
   		for (int i = 0; i < json.size(); i++)
        {
      	  EmployementHistory employementHistory=new EmployementHistory();
      	  
      	  JSONObject objects = (JSONObject)json.get(i);
      	  employementHistory.setCompany((String)objects.get("companyName"));
      	 
      	  //employementHistory.setDesignation((String)objects.get("designation"));
      	  Designation design=new Designation();
      	  if(!((String)objects.get("designation")).equals(""))
      	  design.setDesiId(Long.valueOf((String)objects.get("designation")));
      	  employementHistory.setDesignationId(design);
      	  com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
      	  emp.setSid(employeeEntity.getSid());
      	  employementHistory.setEmpId(emp);
      	  employementHistory.setEnable(true);
            
        
            Role role=new Role(); 
            String roleId=(String)objects.get("role");
            if(!roleId.equals(""))
            role.setId(new Long(roleId));
            employementHistory.setRoleId(role);
            employementHistory.setStartDate((String)objects.get("startDate"));
            employementHistory.setEndDate((String)objects.get("endDate"));
	        if(null!=(String)objects.get("id") && !((String)objects.get("id")).equals("on"))
            employementHistory.setId(Long.valueOf((String)objects.get("id")));
            
            
      	  employmentHistoryList.add(employementHistory);
      	  
        }  
   		employmentHistoryService.saveEmployementHistory(employmentHistoryList);
   		  actionResponse.setRenderParameter("render", "saveExitSuccess-view");
   	  }
   	  else
   	  {
   		for (int i = 0; i < json.size(); i++)
        {
      	  EmployementHistory employementHistory=new EmployementHistory();
      	  
      	  JSONObject objects = (JSONObject)json.get(i);
      	  employementHistory.setCompany((String)objects.get("companyName"));
      	 
      	  //employementHistory.setDesignation((String)objects.get("designation"));
      	  Designation design=new Designation();
      	  if(!((String)objects.get("designation")).equals(""))
      	  design.setDesiId(Long.valueOf((String)objects.get("designation")));
      	  employementHistory.setDesignationId(design);
      	  com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
      	  emp.setSid(employeeEntity.getSid());
      	  employementHistory.setEmpId(emp);
      	  employementHistory.setEnable(true);
            
        
            Role role=new Role(); 
            String roleId=(String)objects.get("role");
            if(!roleId.equals(""))
            role.setId(new Long(roleId));
            employementHistory.setRoleId(role);
            employementHistory.setStartDate((String)objects.get("startDate"));
            employementHistory.setEndDate((String)objects.get("endDate"));
	        if(null!=(String)objects.get("id") && !((String)objects.get("id")).equals("on"))
            employementHistory.setId(Long.valueOf((String)objects.get("id")));
            
            
      	  employmentHistoryList.add(employementHistory);
        }  
        
        
        employmentHistoryService.saveEmployementHistory(employmentHistoryList);
       actionResponse.setRenderParameter("render", "projectDetails-view");

   	  }
      
      
      
      
      
      
    }
    catch (ParseException e)
    {
      e.printStackTrace();
      actionResponse.setRenderParameter("render", "failed-view");
    } catch (PortalException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SystemException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
  
  @ActionMapping(params={"action=familyDetails"})
  public void familyDetailsAction(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("familyDetails");
    }
    try
    {
    	User currentUser=null;
    	currentUser = PortalUtil.getUser(actionRequest);
		System.out.println(currentUser.getRoles());
		
		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
		List<com.hrms.entity.FamilyDetails> familyDetails=null;
		if(null!=employeeEntity)
		{
			 //familyDetails=familyDetailsService.getFamilyDetailsByEmpId(""+employeeEntity.getEmpId());
			 familyDetails=familyDetailsService.getFamilyDetailsByEmpSid(employeeEntity.getSid());

		}
		 
		 FamilyModel familyModel=null;
		
		      /*if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("familyModel") != null)
		      {
		        familyModel = (FamilyModel)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("familyModel");
		        
		        
		      }
		      else
		      {*/
		    	  
		        familyModel = new FamilyModel();
		        List<FamilyDetails> familyDetailList = new ArrayList<FamilyDetails>();
		      
		        if(null!=familyDetails && familyDetails.size()>0)
		        {
		        	for(com.hrms.entity.FamilyDetails familyDetails2:familyDetails)
		        	{
		        		
		        		FamilyDetails familyDetailsModel=new FamilyDetails();
		        		familyDetailsModel.setfName(familyDetails2.getfName());
		        		familyDetailsModel.setmName(familyDetails2.getmName());
		        		familyDetailsModel.setlName(familyDetails2.getlName());
		        		if(null!=familyDetails2.getRelationId())
		        		{
			        		familyDetailsModel.setRelation(""+familyDetails2.getRelationId().getId());

		        		}
		        		if(null!=familyDetails2.getGenderId()){
			        		familyDetailsModel.setGender(""+familyDetails2.getGenderId().getId());

		        		}
		        		familyDetailsModel.setDateOfBirth(familyDetails2.getDateOfBirth());
		        		familyDetailsModel.setEmailId(familyDetails2.getEmailId());
		        		familyDetailsModel.setMobileNO(familyDetails2.getMobileNo());
		        		familyDetailsModel.setOccupation(familyDetails2.getOccupation());
		        		familyDetailsModel.setPassportNO(familyDetails2.getPasport());
		        		familyDetailsModel.setAadharNo(familyDetails2.getAadharNo());
		        		familyDetailsModel.setAdharFileName(familyDetails2.getAdharFileName());
		        		
						familyDetailsModel.setSlNo(""+familyDetails2.getSid());
		        		familyDetailsModel.setEnable(familyDetails2.isEnable());
		        		familyDetailList.add(familyDetailsModel);
		        	}
		        }
		        familyModel.setFamily(familyDetailList);
		        
		        
		        System.out.println("*********************************************");
		     // }
		      
		      	familyModel.setGenderMap(genderMap);
		        familyModel.setRelationTypeMap(relationTypeMap);
		        map.put("familyModel", familyModel);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    
    
    actionResponse.setRenderParameter("render", "familyDetails-view");
  }
  
  @RenderMapping(params={"render=familyDetails-view"})
  public String familyDetailsView(RenderRequest request, Map<String, Object> map)
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("familyDetailsView");
    }
    try
    {
    	User currentUser=null;
    	currentUser = PortalUtil.getUser(request);
		System.out.println(currentUser.getRoles());
		
		com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
		List<com.hrms.entity.FamilyDetails> familyDetails=null;
		if(null!=employeeEntity)
		{
			 //familyDetails=familyDetailsService.getFamilyDetailsByEmpId(""+employeeEntity.getEmpId());
			 familyDetails=familyDetailsService.getFamilyDetailsByEmpSid(employeeEntity.getSid());

		}
		 
		 FamilyModel familyModel=null;
		
		      if (PortalUtil.getHttpServletRequest(request).getSession().getAttribute("familyModel") != null)
		      {
		        familyModel = (FamilyModel)PortalUtil.getHttpServletRequest(request).getSession().getAttribute("familyModel");
		        
		        
		      }
		      else
		      {
		    	  
		        familyModel = new FamilyModel();
		        List<FamilyDetails> familyDetailList = new ArrayList<FamilyDetails>();
		      
		        if(null!=familyDetails && familyDetails.size()>0)
		        {
		        	for(com.hrms.entity.FamilyDetails familyDetails2:familyDetails)
		        	{
		        		
		        		FamilyDetails familyDetailsModel=new FamilyDetails();
		        		familyDetailsModel.setfName(familyDetails2.getfName());
		        		familyDetailsModel.setmName(familyDetails2.getmName());
		        		familyDetailsModel.setlName(familyDetails2.getlName());
		        		if(null!=familyDetails2.getRelationId())
		        		{
			        		familyDetailsModel.setRelation(""+familyDetails2.getRelationId().getId());

		        		}
		        		if(null!=familyDetails2.getGenderId()){
			        		familyDetailsModel.setGender(""+familyDetails2.getGenderId().getId());

		        		}
		        		familyDetailsModel.setDateOfBirth(familyDetails2.getDateOfBirth());
		        		familyDetailsModel.setEmailId(familyDetails2.getEmailId());
		        		familyDetailsModel.setMobileNO(familyDetails2.getMobileNo());
		        		familyDetailsModel.setOccupation(familyDetails2.getOccupation());
		        		familyDetailsModel.setPassportNO(familyDetails2.getPasport());
		        		familyDetailsModel.setAadharNo(familyDetails2.getAadharNo());
		        		familyDetailsModel.setAdharFileName(familyDetails2.getAdharFileName());
		        		
						familyDetailsModel.setSlNo(""+familyDetails2.getSid());
		        		familyDetailsModel.setEnable(familyDetails2.isEnable());
		        		familyDetailList.add(familyDetailsModel);
		        	}
		        }
		        familyModel.setFamily(familyDetailList);
		        
		        
		        System.out.println("*********************************************");
		      }
		      
		      	familyModel.setGenderMap(genderMap);
		        familyModel.setRelationTypeMap(relationTypeMap);
		        map.put("familyModel", familyModel);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return "familyDetails";
  }
  
  @ActionMapping(value="saveFamilyInfo")
  public void saveFamilyInfo(@ModelAttribute("familyModel") FamilyModel familymodel, ActionRequest actionRequest,ActionResponse actionResponse, Map<String, Object> map)throws FileNotFoundException
  {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("***************************saveFamilyInfo*****************************");
    }
    UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);  
    String inputString = uploadRequest.getParameter("familyDetails1");
    JSONParser parser = new JSONParser();
    List<com.hrms.entity.FamilyDetails> familyDetailsEntityList=new ArrayList<com.hrms.entity.FamilyDetails>();
    List<FamilyDetails> familyDetailList = new ArrayList<FamilyDetails>();
    try
    {
      JSONArray json = (JSONArray)parser.parse(inputString);
      
      // saveAndExit
	  if(uploadRequest.getParameter("saveandExitName").equals("saveAndExit"))
	  {
   		  System.out.println("save and exit");
   		  
 		  for (int i = 0; i < json.size(); i++)
   	      {

   	    	  com.hrms.entity.FamilyDetails family=new com.hrms.entity.FamilyDetails();
   	        JSONObject objects = (JSONObject)json.get(i);
   	        
   	        family.setfName((String)objects.get("firstName"));
   	        family.setmName((String)objects.get("middleName"));
   	        family.setlName((String)objects.get("lastName"));
   	        family.setDateOfBirth((String)objects.get("dateOfBirth"));
   	        family.setEmailId((String)objects.get("email"));
			if(null!=(String)objects.get("relation") && !( (String)objects.get("relation")).equals("0"))
			{
				Relationship relationship=new Relationship();
				String relId1= (String)objects.get("relation");
				Long relId = Long.valueOf(relId1);
				relationship.setId(relId);
				family.setRelationId(relationship);
			}				
  	        
				Gender gender=new Gender();  
				Long genderId=new Long(1);//change later as of now its hard coaded
				gender.setId(genderId);
				family.setGenderId(gender);

			
   	        family.setMobileNo((String)objects.get("mobileNo"));
   	        family.setOccupation((String)objects.get("occupation"));
   	        family.setPasport((String)objects.get("passport"));
   	        family.setAadharNo((String)objects.get("adhar"));
   	        

   	        
   			String AdharDocumentFileName = uploadRequest.getFileName("adhaarDocument");
   			if(AdharDocumentFileName!="")
   			{
   				try {
   						String fileInputName = "adhaarDocument";
   						long sizeInBytes = uploadRequest.getSize(fileInputName);
   						if (uploadRequest.getSize(fileInputName) == 0)
   							{ 
   								throw new Exception("Received file is 0 bytes!"); 
   							}
   				  
   						// Get the upload file as a file.
   						File uploadedFile = uploadRequest.getFile(fileInputName,true);
   						FileInputStream adharFileInput = new FileInputStream(uploadedFile);
   						OutputBlob adharBlobOutput = new OutputBlob(adharFileInput,uploadedFile.length());
   						
   						family.setAdhaarDocument(adharBlobOutput);
   						family.setAdharFileName(AdharDocumentFileName);
   						if(!((String)objects.get("id")).equals("on"))
   					        family.setSid(Long.valueOf((String)objects.get("id")));
   					}
   					catch (Exception e) {
   						System.out.println(e);
   					}
   			}
		PortletSession session = actionRequest.getPortletSession();
		com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
		
		System.out.println(employee);
     
            family.setEmpId(employee);
   	        family.setEnable(true);
   	        familyDetailsEntityList.add(family);
   	      }
 		  
   	      familyDetailsService.saveFamilyDetails(familyDetailsEntityList);
   	      
   	      FamilyModel familyModel = new FamilyModel();
   	      familyModel.setFamily(familyDetailList);
   	      PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("familyModel", familyModel);
   	    
   		  
   		  actionResponse.setRenderParameter("render", "saveExitSuccess-view");
   	 	}
   	 	else
   	 	{
   		  for (int i = 0; i < json.size(); i++)
   	      {
   	        //FamilyDetails familyDetails = new FamilyDetails();
   	    	  com.hrms.entity.FamilyDetails family=new com.hrms.entity.FamilyDetails();
   	        JSONObject objects = (JSONObject)json.get(i);
   	        
   	        /*familyDetails.setSlNo("1");
   	        familyDetails.setfName((String)objects.get("firstName"));
   	        familyDetails.setmName((String)objects.get("middleName"));
   	        familyDetails.setlName((String)objects.get("lastName"));
   	        familyDetails.setRelation((String)objects.get("relation"));
   	        familyDetails.setDateOfBirth((String)objects.get("dateOfBirth"));
   	        familyDetails.setGender((String)objects.get("gender"));
   	        familyDetails.setMobileNO((String)objects.get("mobileNo"));
   	        familyDetails.setOccupation((String)objects.get("occupation"));
   	        familyDetails.setPassportNO((String)objects.get("passport"));
   	        familyDetails.setEmailId((String)objects.get("email"));
   	        familyDetailList.add(familyDetails);*/
   	        family.setfName((String)objects.get("firstName"));
   	        family.setmName((String)objects.get("middleName"));
   	        family.setlName((String)objects.get("lastName"));
   	        family.setDateOfBirth((String)objects.get("dateOfBirth"));
   	        family.setEmailId((String)objects.get("email"));
   	        
   	        Relationship relationship=new Relationship();
        String relId1= (String)objects.get("relation");
        Long relId = Long.valueOf(relId1);
   	        relationship.setId(relId);
   	        family.setRelationId(relationship);
   	        
   	        Gender gender=new Gender();
   	       // Long genderId=new Long((String)objects.get("gender"));
   	        Long genderId=new Long(1);//change later as of now its hard coaded
   	        gender.setId(genderId);
   	        family.setGenderId(gender);
   	        family.setMobileNo((String)objects.get("mobileNo"));
   	        family.setOccupation((String)objects.get("occupation"));
   	        family.setPasport((String)objects.get("passport"));
        family.setAadharNo((String)objects.get("adhar"));
        	if(!((String)objects.get("id")).equals("on"))
        family.setSid(Long.valueOf((String)objects.get("id")));
/*        try {
			File adharDocument = uploadRequest.getFile("adhaarDocument",true);
			FileInputStream adharFileInput = new FileInputStream(adharDocument);
			OutputBlob adharBlobOutput = new OutputBlob(adharFileInput,adharDocument.length());
			String AdharDocumentFileName = uploadRequest.getFileName("adhaarDocument");
			family.setAdhaarDocument(adharBlobOutput);
			family.setAdharFileName(AdharDocumentFileName);
        	}
			catch (Exception e) {
				System.out.println(e);
			}*/
   	        
   			String AdharDocumentFileName = uploadRequest.getFileName("adhaarDocument");
   			if(AdharDocumentFileName!="")
   			{
   				try {
   						String fileInputName = "adhaarDocument";
   						long sizeInBytes = uploadRequest.getSize(fileInputName);
   						if (uploadRequest.getSize(fileInputName) == 0)
   							{ 
   								throw new Exception("Received file is 0 bytes!"); 
   							}
   				  
   						// Get the upload file as a file.
   						File uploadedFile = uploadRequest.getFile(fileInputName,true);
   						FileInputStream adharFileInput = new FileInputStream(uploadedFile);
   						OutputBlob adharBlobOutput = new OutputBlob(adharFileInput,uploadedFile.length());
   						
   						family.setAdhaarDocument(adharBlobOutput);
   						family.setAdharFileName(AdharDocumentFileName);
   					}
   					catch (Exception e) {
   						System.out.println(e);
   					}
   			}
		PortletSession session = actionRequest.getPortletSession();
		com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
		
		System.out.println(employee);
	//	String empId = employee.getEmpId();
     //   com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
     
            family.setEmpId(employee);
   	        family.setEnable(true);
   	        familyDetailsEntityList.add(family);
   	      }
   	      familyDetailsService.saveFamilyDetails(familyDetailsEntityList);
   	      FamilyModel familyModel = new FamilyModel();
   	      familyModel.setFamily(familyDetailList);
   	      PortalUtil.getHttpServletRequest(actionRequest).getSession().setAttribute("familyModel", familyModel);
   	      actionResponse.setRenderParameter("render", "employmentDetails-view");
   	 	
   	 	}
      
    
    }
    catch (ParseException e)
    {
      e.printStackTrace();
      actionResponse.setRenderParameter("render", "failed-view");
    }
  }


	@ActionMapping(params={"action=documentDetails"})
	public void documentDetails(Map<String, Object> map, ActionRequest actionRequest, ActionResponse actionResponse)
	{
		actionResponse.setRenderParameter("render", "documentDetails-view");
	}
	 @RenderMapping(params={"render=documentDetails-view"})
	  public String loadDocuments(RenderRequest request,RenderResponse response, Map<String, Object> map)
	  {
			Document document = new Document();
			
			 PortletSession session = request.getPortletSession();
			com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
			
			System.out.println(employee);
			    
			if(null!=employee)
			{
				String empId = employee.getEmpId();
			    
				document=documentService.getDocumentByEmployeeId(empId);
			}
			
			
			DocumentModel documentModel = new DocumentModel();
			
			if(null!=document)
			{
				documentModel.setUanNo(document.getUanNumber());
				documentModel.setPfNo(document.getPfNumber());
				documentModel.setAadharNo(document.getAdhaarNumber());
				documentModel.setDrivingLicenceNo(document.getDrivingNumber());
				documentModel.setPancardNo(document.getPanNumber());
				documentModel.setEducationDetailsNo(document.getEducationNumber());
				documentModel.setCertificateNo(document.getCertificateNumber());
				documentModel.setVoterIdNo(document.getVoterIdNumber());
				documentModel.setPassportNo(document.getPassportNumber());
				
				documentModel.setUanFileName(document.getUanFileName());
				documentModel.setPfFileName(document.getPfFileName());
				documentModel.setAdhaarFileName(document.getAdhaarFileName());
				documentModel.setDrivingLicenceFileName(document.getDrivingLicenceFileName());
				documentModel.setPanNoFileName(document.getPanNoFileName());
				documentModel.setEducationDetailsFileName(document.getEducationDetailsFileName());
				documentModel.setCertificateFileName(document.getCertificateFileName());
				documentModel.setVoterIdFileName(document.getVoterIdFileName());
				documentModel.setPassportFileName(document.getPassportFileName());
				
			}
			
			map.put("documentModel", documentModel);
		 
		 return "Documnet";
	  }
	 
	 
	 @ActionMapping("savefile")
	  public void saveDocumentDetails(@ModelAttribute("documentModel") DocumentModel documentDetails,ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map)throws FileNotFoundException
	  {

			
		 //   PortletSession session = actionRequest.getPortletSession();
		    com.hrms.entity.Employee employee = new com.hrms.entity.Employee();
		    //com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
		    
		   // System.out.println(employee);
		   //String empId = employee.getEmpId();
		    	try{
				    	User currentUser = PortalUtil.getUser(actionRequest);
				    	Long userId = currentUser.getUserId();
				    	System.out.println(userId);
				    	// based on the current userID fetch respective employee
				    	employee = employeeService.getEmployeeByUserId(userId.toString());
					} catch (Exception e)
			    	  {
						// TODO Auto-generated catch block
						e.printStackTrace();
					   }
			    
					Document document=new Document();		    
		try
		  {
					//System.out.println("documentDetails "+documentDetails);
					
					 //actionRequest.getParameter("uanNo");
			
					UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
					
				//**************************************UAN No document************************
					String uanDocs = uploadRequest.getFileName("uanFileName");
					if(uanDocs!="")
					{
						OutputBlob uanOB=uploadDocument(actionRequest,"uanFileName",uploadRequest);
						
						 document.setUanFileName(uanDocs);
						 document.setUanNo((Blob)uanOB);
						
						 document.setUanNumber(uploadRequest.getParameter("uanNo"));
					}
					
				//**************************************PF document*****************************
					String pfDocs = uploadRequest.getFileName("pfFileName");
					if(pfDocs!="")
					{
						OutputBlob pfOB=uploadDocument(actionRequest,"pfFileName",uploadRequest);
						
						 document.setPfFileName(pfDocs);
						 document.setPfNo((Blob)pfOB);	 
						
						 document.setPfNumber(uploadRequest.getParameter("pfNo"));
					}
					
				//**************************************Aadhar document**************************
					String aadharDocs = uploadRequest.getFileName("adhaarFileName");
					if(aadharDocs!="")
					{
						OutputBlob aadharOB=uploadDocument(actionRequest,"adhaarFileName",uploadRequest);
						
						 document.setAdhaarFileName(aadharDocs);
						 document.setAdhaarNo((Blob)aadharOB);
						
						 document.setAdhaarNumber(uploadRequest.getParameter("aadharNo"));	
					}
					 
				//**************************************Driving License document*****************
					String drivingLicenceDocs = uploadRequest.getFileName("drivingLicenceFileName");
					if(drivingLicenceDocs!="")
					{
						OutputBlob drivingLicenceOB=uploadDocument(actionRequest,"drivingLicenceFileName",uploadRequest);
						 
						 document.setDrivingLicenceFileName(drivingLicenceDocs);
						 document.setDrivingNo((Blob)drivingLicenceOB);
						 
						 document.setDrivingNumber(uploadRequest.getParameter("drivingLicenceNo"));
					}
					 
				//**************************************Pan No document****************************
					String pancardDocs = uploadRequest.getFileName("panNoFileName");
					if(pancardDocs!="")
					{
						OutputBlob pancardOB=uploadDocument(actionRequest,"panNoFileName",uploadRequest);
						
						 document.setPanNoFileName(pancardDocs);
						 document.setPanNo((Blob)pancardOB);
						 
						 document.setPanNumber(uploadRequest.getParameter("pancardNo"));
					}
				
				//**************************************Education document*************************
					String educationDetailDocs = uploadRequest.getFileName("educationDetailsFileName");
					if(educationDetailDocs!="")
					{
						OutputBlob educationDetailsOB=uploadDocument(actionRequest,"educationDetailsFileName",uploadRequest);
						
						 document.setEducationDetailsFileName(educationDetailDocs);
						 document.setEducationDetailsDoc((Blob)educationDetailsOB);
						 
						 document.setEducationNumber(uploadRequest.getParameter("educationDetailsFileNo"));
					}
					 
				//**************************************Certificate document*****************
					
					String certificateDocs = uploadRequest.getFileName("certificateFileName");
					if(certificateDocs!="")
					{
						OutputBlob certificateOB=uploadDocument(actionRequest,"certificateFileName",uploadRequest);
						 
						 document.setCertificateFileName(certificateDocs);
						 document.setCretificateDoc((Blob)certificateOB);
						 
						 document.setCertificateNumber(uploadRequest.getParameter("certificateNumber"));
					}
	
				//**************************************Passport document*****************
					String passportDocs = uploadRequest.getFileName("passportFileName");
					if(passportDocs!="")
					{
						OutputBlob passportOB=uploadDocument(actionRequest,"passportFileName",uploadRequest);
						
						 document.setPassportFileName(passportDocs);
						 document.setPassportDoc((Blob)passportOB);
						 
						 document.setPassportNumber(uploadRequest.getParameter("passportNo"));
					}
					 
				//**************************************Voter ID document*****************
					String voterIdDocs = uploadRequest.getFileName("voterIdFileName");
					if(voterIdDocs!="")
					{
						OutputBlob voterIdOB=uploadDocument(actionRequest,"voterIdFileName",uploadRequest);
						 
						 document.setVoterIdFileName(voterIdDocs);
						 document.setVoterIdDoc((Blob)voterIdOB);
						 
						 document.setVoterIdNumber(uploadRequest.getParameter("voterIdNo"));			
					}
	
					 document.setEmpId(employee);
					 //System.out.println(document.getEmpId());
					 
				//******************************************************************************************
				//**************************************Get the old documents that employee uploaded previously*************************************
						Document oldDocumentEntity = new Document();
						
						PortletSession session = actionRequest.getPortletSession();
						com.hrms.entity.Employee employeeEntity = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
						
						System.out.println(employeeEntity);
						    
						if(null!=employeeEntity)
						{
							String employeeId = employeeEntity.getEmpId();
							
							
							oldDocumentEntity = documentService.getDocumentByEmployeeId(employeeId);
							
							if(uanDocs =="")
							{
								 document.setUanFileName(oldDocumentEntity.getUanFileName());
								 document.setUanNo(oldDocumentEntity.getUanNo());
								 document.setUanNumber(oldDocumentEntity.getUanNumber());
							}

							if(pfDocs=="")
							{
								 document.setPfFileName(oldDocumentEntity.getPfFileName());
								 document.setPfNo(oldDocumentEntity.getPfNo());	 	
								 document.setPfNumber(oldDocumentEntity.getPfNumber());
							}

							if(aadharDocs=="")
							{
								 document.setAdhaarFileName(oldDocumentEntity.getAdhaarFileName());
								 document.setAdhaarNo(oldDocumentEntity.getAdhaarNo());
								 document.setAdhaarNumber(oldDocumentEntity.getAdhaarNumber());	
							}
		
							if(drivingLicenceDocs=="")
							{
								 document.setDrivingLicenceFileName(oldDocumentEntity.getDrivingLicenceFileName());
								 document.setDrivingNo(oldDocumentEntity.getDrivingNo());
								 document.setDrivingNumber(oldDocumentEntity.getDrivingNumber());
							}

							if(pancardDocs=="")
							{
								 document.setPanNoFileName(oldDocumentEntity.getPanNoFileName());
								 document.setPanNo(oldDocumentEntity.getPanNo()); 
								 document.setPanNumber(oldDocumentEntity.getPanNumber());
							}

							if(educationDetailDocs=="")
							{
								 document.setEducationDetailsFileName(oldDocumentEntity.getEducationDetailsFileName());
								 document.setEducationDetailsDoc(oldDocumentEntity.getEducationDetailsDoc());
								 document.setEducationNumber(oldDocumentEntity.getEducationNumber());
							}

							if(certificateDocs=="")
							{
								 document.setCertificateFileName(oldDocumentEntity.getCertificateFileName());
								 document.setCretificateDoc(oldDocumentEntity.getCretificateDoc());
								 document.setCertificateNumber(oldDocumentEntity.getCertificateNumber());
							}

							if(passportDocs=="")
							{
								 document.setPassportFileName(oldDocumentEntity.getPassportFileName());
								 document.setPassportDoc(oldDocumentEntity.getPassportDoc());
								 document.setPassportNumber(oldDocumentEntity.getPassportNumber());
							}

							if(voterIdDocs=="")
							{
								 document.setVoterIdFileName(oldDocumentEntity.getVoterIdFileName());
								 document.setVoterIdDoc(oldDocumentEntity.getVoterIdDoc()); 
								 document.setVoterIdNumber(oldDocumentEntity.getVoterIdNumber());	
							}
						}
						
						 
				//*********************************************Save all documents in the DB*********************************************
			
						documentService.save(document);
						actionResponse.setRenderParameter("render", "success-view");	
			}
			catch(Exception e)
			{
				e.printStackTrace();
				actionResponse.setRenderParameter("render", "failed-view");
		}
	  }


	private OutputBlob uploadDocument(ActionRequest actionRequest,String fileInputName,UploadPortletRequest uploadRequest) throws Exception
	{
		// TODO Auto-generated method stub
		//UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		 FileInputStream fileInput=null;
		 OutputBlob blobOutput=null;
		
		   long sizeInBytes = uploadRequest.getSize(fileInputName);
		  if (uploadRequest.getSize(fileInputName) == 0) { 
			    throw new Exception("Received file is 0 bytes!"); 
			   }
			  
			   // Get the uploaded file as a file.
			   File uploadedFile = uploadRequest.getFile(fileInputName);
			   
			   
			   //uploadedFile.
			   
		   System.out.println("received file :");
		   String sourceFileName = uploadRequest.getFileName(fileInputName);
			  
		   // Where should we store this file?
		   File folder = new File(baseDir);
		  
		   // Check minimum 1GB storage space to save new files...
		   
		  /* if (folder.getUsableSpace() < ONE_GB) {
		    throw new Exception("Out of disk space!");
		   }*/
		  
		   // This is our final file path.
		   File filePath = new File(folder.getAbsolutePath() + File.separator + sourceFileName);
		  
		   // Move the existing temporary file to new location.
		    
		   FileUtils.copyFile(uploadedFile, filePath);
		   System.out.println("file uploaded");
		   
		  
		  
		
			fileInput = new FileInputStream(uploadedFile);
			blobOutput = new OutputBlob(fileInput,uploadedFile.length());
		
		  // System.out.println("writing excel data to DB:"+qfdi.saveQuoteToDB(filePath));
		   //fileInput.close();
		   
		   return blobOutput;
	}
    
	  @RenderMapping(params={"render=success-view"})
	  public String success()
	  			{
		   			if (LOGGER.isTraceEnabled()) {
		   				LOGGER.trace("Submit Success");
		   			}
		   			return "submitSuccess";
	  			}
	  
	  @RenderMapping(params={"render=failed-view"})
	  public String failed()
	  			{
		   			if (LOGGER.isTraceEnabled()) {
		   				LOGGER.trace("Submit Failure");
		   			}
		   			return "submitFailed";
	  			}
	  
	  //*******************************************************************Uploaded Documents Download***************************************

	    @ResourceMapping(value = "downloadFileURL")
		//public void downloadEmployeeDocuments(@ModelAttribute("documentModel") DocumentModel documentModel, ResourceRequest request,ResourceResponse response, Map<String, Object> map)
		public void downloadEmployeeDocuments(ResourceRequest request,ResourceResponse response, Map<String, Object> map)

	    		throws IOException, SQLException {
			
	    	UploadPortletRequest downloadRequest = PortalUtil.getUploadPortletRequest(request);
	    	
	    	//System.out.println(child);
			Document document = new Document();
			
			PortletSession session = request.getPortletSession();
			com.hrms.entity.Employee employee = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
			    System.out.println(employee);
			  String empId = employee.getEmpId();
			  
			  String downloadNumber = downloadRequest.getParameter("downloadNumber");
			 // String downloadNumber = documentModel.getDownloadNumber();
				System.out.println(" downloadFileURL downloadnum:"+downloadNumber);
				
			  document=documentService.getDocumentByEmployeeId(empId);
			  			  
			  Blob uanNoBlob = document.getUanNo();
			  Blob pfNoBlob = document.getPfNo();
			  Blob adhaarNoBlob = document.getAdhaarNo();
			  Blob drivingNoBlob = document.getDrivingNo();
			  Blob panNoBlob = document.getPanNo();
			  Blob educationDetailsDocBlob = document.getEducationDetailsDoc();
			  Blob certificateDocBlob = document.getCretificateDoc();
			  Blob passportDocBlob = document.getPassportDoc();
			  Blob voterIdDocBlob = document.getVoterIdDoc();
			  			  
			  Blob blobData = null; 
			  
			  response.setContentType("application/octet-stream");
				response.addProperty(HttpHeaders.CACHE_CONTROL,
						"max-age=3600, must-revalidate");


			  
			  if(uanNoBlob!= null && document.getUanNumber().equals(downloadNumber))
			  {
				 	blobData = uanNoBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getUanFileName()); 								
			  }else if(pfNoBlob!= null && document.getPfNumber().equals(downloadNumber))
			  {
				  	blobData = pfNoBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getPfFileName());
			  }else if(adhaarNoBlob!= null && document.getAdhaarNumber().equals(downloadNumber))
			  {
				  	blobData = adhaarNoBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getAdhaarFileName()); 
			  }else if(drivingNoBlob!= null && document.getDrivingNumber().equals(downloadNumber))
			  {
				  	blobData = drivingNoBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getDrivingLicenceFileName()); 
			  }else if(panNoBlob!= null && document.getPanNumber().equals(downloadNumber))
			  {
				  	blobData = panNoBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getPanNoFileName()); 
			  }else if(educationDetailsDocBlob!= null && document.getEducationNumber().equals(downloadNumber))
			  {
				  	blobData = educationDetailsDocBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getEducationDetailsFileName()); 
			  }else if(certificateDocBlob!= null && document.getCertificateNumber().equals(downloadNumber))
			  {
				  	blobData = certificateDocBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getCertificateFileName()); 
			  }else if(passportDocBlob!= null && document.getPassportNumber().equals(downloadNumber))
			  {
				  	blobData = passportDocBlob;
					response.addProperty(
							HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename="
								+ document.getPassportFileName()); 
			  }else if(voterIdDocBlob!= null && document.getVoterIdNumber().equals(downloadNumber))
				  {
					  	blobData = voterIdDocBlob;
						response.addProperty(
								HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename="
									+ document.getVoterIdFileName()); 
				  }
							
				byte[] fileBytes = null;
				try {
						fileBytes = blobData.getBytes(1, (int) blobData.length());
					} catch (SQLException e) 
						{
							e.printStackTrace();
						}

				
				OutputStream out = response.getPortletOutputStream();
				InputStream in = new ByteArrayInputStream(fileBytes);

				byte[] buffer = new byte[4096];
				int len;
				
			try{
					while ((len = in.read(buffer)) != -1)
					{
						out.write(buffer, 0, len);
					}
				}
			catch(Exception e)
			{
				e.printStackTrace();
			}



				out.flush();
				in.close();
				out.close();

			}
	    
 //*******************************************************************CERTIFICATION&TRAINING DETAILS MAPPING START***************************************
	    
	    @ActionMapping(params = "action=certificationTraining")
	    public void certificationtrainingDetailsAction(ActionRequest actionRequest,ActionResponse actionResponse, Map<String, Object> map){
	     if (LOGGER.isTraceEnabled()) {
	            LOGGER.trace("otherDetails");
	        }
	     

	     
	        if (LOGGER.isTraceEnabled()) {
	            LOGGER.trace("certificationtrainingDetailsView");
	        }
	        
	       
	        User currentUser=null;
	       
			try {
				
				
				currentUser = PortalUtil.getUser(actionRequest);
				System.out.println(currentUser.getRoles());
				
				com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
				
				List<CertificationDetails> certifications=null;
				if(null!=employeeEntity)
				{
					certifications=certificateDetailsService.getCertificationByEmpId(""+employeeEntity.getSid());

				}
				List<Certification>  certList=new ArrayList<Certification>();
				
				if(null!=certifications && certifications.size()>0)
				{
					for(CertificationDetails certificationDetails:certifications)
					{
						
						Certification certification=new Certification();
						certification.setsNo(""+certificationDetails.getId());
						certification.setCertificationDate(certificationDetails.getCertificationDate());
						if(null!=certificationDetails.getCertificationNameType())
						{
							certification.setCertificationtype(""+certificationDetails.getCertificationNameType().getCerttypeID());

						}
						if(null!=certificationDetails.getCertificationType())
						{
							certification.setCerttype(""+certificationDetails.getCertificationType().getCerttypeID());

						}
						certification.setCertificationVersion(certificationDetails.getCertificationVersion());
						certification.setCourseDuration(certificationDetails.getCourseDuration());
						certification.setCertificationPercentage(certificationDetails.getCertificationPercentage());
						//certification.setCertification(certificationDetails.getCertName());
						certification.setInstituteName(certificationDetails.getInstituteName());
						certification.setInstituteLocation(certificationDetails.getInstituteLocation());
						certification.setTrainedCourseName(certificationDetails.getTrainedCourseName());
						certification.setEnable(certificationDetails.isEnable());
						certification.setSuccess(certificationDetails.isSuccess());
						certification.setId(""+certificationDetails.getId());

						certList.add(certification);
						
					}
				}
				
				 map.put("certList", certList); 
				
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
			
	        //remove hard coaded value
			 Certification  certification = new Certification();
	        
	       certificationTypeList.put("1","Educational");
	       certificationTypeList.put("2","Business");
	       certificationTypeList.put("3","Technology");
	       certificationTypeList.put("4","AnyOtherCertifications");
	       certification.setCertificationTypeList(certificationTypeList);
	       map.put("certificationTypeList", certificationTypeList); 
	       
	       certificationList.put("1","JAVA");
	       certificationList.put("2","SAP");
	       certificationList.put("3","DATABASE");
	       certificationList.put("4","TESTING");
	       certification.setCertificationList(certificationList);
	       
	       
	       map.put("certificationList", certificationList);
	       map.put("certification", certification);
	       
	    
	     
	     actionResponse.setRenderParameter("render", "certificationtrainingDetails-view");
	     
	    }
	    
	    
	    @RenderMapping(params = "render=certificationtrainingDetails-view")
	    public String certificationtrainingDetailsView(RenderRequest request, Map<String, Object> map) {
	     
	        if (LOGGER.isTraceEnabled()) {
	            LOGGER.trace("certificationtrainingDetailsView");
	        }
	        
	       
	        User currentUser=null;
	       
			try {
				
				
				currentUser = PortalUtil.getUser(request);
				System.out.println(currentUser.getRoles());
				
				com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
				List<CertificationDetails> certifications=null;
				if(null!=employeeEntity)
				{
					 certifications=certificateDetailsService.getCertificationByEmpId(""+employeeEntity.getSid());

				}
				List<Certification>  certList=new ArrayList<Certification>();
				
				if(null!=certifications && certifications.size()>0)
				{
					for(CertificationDetails certificationDetails:certifications)
					{
						
						Certification certification=new Certification();
						certification.setsNo(""+certificationDetails.getId());
						certification.setCertificationDate(certificationDetails.getCertificationDate());
						if(null!=certificationDetails.getCertificationNameType())
						{
							certification.setCertificationtype(""+certificationDetails.getCertificationNameType().getCerttypeID());

						}
						if(null!=certificationDetails.getCertificationType())
						{
							certification.setCerttype(""+certificationDetails.getCertificationType().getCerttypeID());

						}
						certification.setCertificationVersion(certificationDetails.getCertificationVersion());
						certification.setCourseDuration(certificationDetails.getCourseDuration());
						certification.setCertificationPercentage(certificationDetails.getCertificationPercentage());
						//certification.setCertification(certificationDetails.getCertName());
						certification.setInstituteName(certificationDetails.getInstituteName());
						certification.setInstituteLocation(certificationDetails.getInstituteLocation());
						certification.setTrainedCourseName(certificationDetails.getTrainedCourseName());
						certification.setEnable(certificationDetails.isEnable());
						certification.setSuccess(certificationDetails.isSuccess());
						certification.setId(""+certificationDetails.getId());
						
						certList.add(certification);
						
					}
				}
				
				 map.put("certList", certList); 
				
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
			
	        //remove hardcoaded value
			 Certification  certification = new Certification();
	        
	       certificationTypeList.put("1","Educational");
	       certificationTypeList.put("2","Business");
	       certificationTypeList.put("3","Technology");
	       certificationTypeList.put("4","AnyOtherCertifications");
	       certification.setCertificationTypeList(certificationTypeList);
	       map.put("certificationTypeList", certificationTypeList); 
	       
	       certificationList.put("1","JAVA");
	       certificationList.put("2","SAP");
	       certificationList.put("3","DATABASE");
	       certificationList.put("4","TESTING");
	       certification.setCertificationList(certificationList);
	       
	       
	      map.put("certificationList", certificationList);
	      
	       map.put("certification", certification);
	       
	        return "CertificationTraining";
	    }
	    
	    
	    
	    //*******************************************************************CERTIFICATION&TRAINING DETAILS MAPPING ENDS*************************************** 
 //*******************************************************************SAVING CERTIFICATION DETAILS STARTS*************************************** 
	    
	    
	    @ActionMapping("saveCertificationTrainingInfo")
	    public void saveCertificationTraining(@ModelAttribute("certification") Certification Certification,BindingResult result, ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map) throws Exception
	    {
	      if (LOGGER.isTraceEnabled()) {
	        LOGGER.trace("***************************saveCertificationInfo*****************************");
	        
	      }
	      System.out.println("hello certification");
	      
	      User currentUser = PortalUtil.getUser(actionRequest);
    	  currentUser.getUserId();
    	  com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
	      
	      String inputString = actionRequest.getParameter("certificationDetails");
	      System.out.println("certificati data "+inputString);
	      JSONParser parser = new JSONParser();
	      //List<Certification> certificationList = new ArrayList<Certification>();
	      List<com.hrms.entity.CertificationDetails> certificationEntityList=new ArrayList<com.hrms.entity.CertificationDetails>();
	      try
	      {
	        
	        
	        
	     // saveAndExit
	     	  if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
	     	  {
	     		  
	     		  System.out.println("save and exit");
	     		  JSONArray json = (JSONArray)parser.parse(inputString);
		  	        for (int i = 0; i < json.size(); i++)
		  	        {
		  	          com.hrms.entity.CertificationDetails certificationEntity=new com.hrms.entity.CertificationDetails();
		  	          
		  	          JSONObject objects = (JSONObject)json.get(i);
		  	          certificationEntity.setCertificationDate((String)objects.get("CertificationDate"));
		  	          certificationEntity.setCertificationVersion((String)objects.get("CertificationVersion"));
		  	          certificationEntity.setCertificationPercentage((String)objects.get("CertificationPercentage"));
		  	          certificationEntity.setTrainedCourseName((String)objects.get("TrainedCourseName"));
		  	          certificationEntity.setInstituteName((String)objects.get("Institutename"));
		  	          certificationEntity.setInstituteLocation((String)objects.get("InstituteLocation"));
		  	          certificationEntity.setCourseDuration((String)objects.get("courseDuration"));
		  	          //certificationEntity.setCertNumber((String)objects.get("CertificationVersion"));
		  	          //certificationEntity.setCertName((String)objects.get("TrainedCourseName"));
		  	          
		  	          CertificationType certificationType=new CertificationType();
		  	          if(null!=(String)objects.get("CertificationType") && !((String)objects.get("CertificationType")).equals(""))
		  	          {
		  	        	  Long relId=new Long((String)objects.get("CertificationType"));
			  	          certificationType.setCerttypeID(relId);
			  	          certificationEntity.setCertificationType(certificationType);
		  	          }
		  	          
		  	          CertificationNameType certificationNameType=new CertificationNameType();
		  	          if(null!=(String)objects.get("Certification") && !((String)objects.get("Certification")).equals(""))
		  	          {
		  	        	  Long certNameId=new Long((String)objects.get("Certification"));
			  	          certificationNameType.setCerttypeID(certNameId);
			  	          certificationEntity.setCertificationNameType(certificationNameType);
		  	          }
		  	          
		  	          
		  	          
		  	         
		  	          
		  	          com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
		  	          emp.setSid(employeeEntity.getSid());
		  	          certificationEntity.setEmpId(emp);
		  	          
		  	          certificationEntity.setEnable(true);
		  	          
		  	        if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
		  	          {
			  	          certificationEntity.setId(Long.valueOf((String)objects.get("slNo")));

		  	          }
		  	          
		  	          certificationEntityList.add(certificationEntity);
		  	          
		  	        }
		  	        
		  	        certificateDetailsService.saveCertification(certificationEntityList);
	     		  
	     		  
	      		  actionResponse.setRenderParameter("render", "saveExitSuccess-view");
	     	  }
	     	  else
	     	  {

	     		 JSONArray json = (JSONArray)parser.parse(inputString);
	  	        for (int i = 0; i < json.size(); i++)
	  	        {
	  	          com.hrms.entity.CertificationDetails certificationEntity=new com.hrms.entity.CertificationDetails();
	  	          
	  	          JSONObject objects = (JSONObject)json.get(i);
	  	          certificationEntity.setCertificationDate((String)objects.get("CertificationDate"));
	  	          certificationEntity.setCertificationVersion((String)objects.get("CertificationVersion"));
	  	          certificationEntity.setCertificationPercentage((String)objects.get("CertificationPercentage"));
	  	          certificationEntity.setTrainedCourseName((String)objects.get("TrainedCourseName"));
	  	          certificationEntity.setInstituteName((String)objects.get("Institutename"));
	  	          certificationEntity.setInstituteLocation((String)objects.get("InstituteLocation"));
	  	          certificationEntity.setCourseDuration((String)objects.get("courseDuration"));
	  	          //certificationEntity.setCertNumber((String)objects.get("CertificationVersion"));
	  	          //certificationEntity.setCertName((String)objects.get("TrainedCourseName"));
	  	          
	  	          CertificationType certificationType=new CertificationType();
	  	          Long relId=new Long((String)objects.get("CertificationType"));
	  	          certificationType.setCerttypeID(relId);
	  	          certificationEntity.setCertificationType(certificationType);
	  	          
	  	          
	  	          CertificationNameType certificationNameType=new CertificationNameType();
	  	          Long certNameId=new Long((String)objects.get("Certification"));
	  	          certificationNameType.setCerttypeID(certNameId);
	  	          certificationEntity.setCertificationNameType(certificationNameType);
	  	          
	  	          
	  	          com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
	  	          emp.setSid(employeeEntity.getSid());
	  	          certificationEntity.setEmpId(emp);
	  	          
	  	          certificationEntity.setEnable(true);
	  	          if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
	  	          {
		  	          certificationEntity.setId(Long.valueOf((String)objects.get("slNo")));

	  	          }
	  	          certificationEntityList.add(certificationEntity);
	  	          
	  	        }
	  	        
	  	        certificateDetailsService.saveCertification(certificationEntityList);
	  	        if (PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee") != null)
	  	        {
	  	  			        Employee employee = (Employee)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute("employee");
	  	  			        System.out.println(employee.getEmployeeId());
	  	  			      //addressEntity.
	  	        }
	  	        
	  	         actionResponse.setRenderParameter("render", "documentDetails-view");

	     	  }
	        
	      }
	      catch (ParseException e)
	      {
	        e.printStackTrace();
	        actionResponse.setRenderParameter("render", "failed-view");
	      }
	    }
	
	    
	    @ActionMapping(params={"action=otherDetails"})
	    public void otherDetails(ActionResponse actionResponse, ActionRequest actionRequest, Map<String, Object> map, Model model)
	    {
	      if (LOGGER.isTraceEnabled()) {
	        LOGGER.trace("otherDetails");
	      }
	      
	      actionResponse.setRenderParameter("render", "otherDetails-view");
	    	     
	    }
	    
	    
	    @RenderMapping(params = "render=otherDetails-view")
	    public String otherDetailsView(Map<String, Object> map) {
	    	
	    	 OtherDetails otherDetails=new OtherDetails();
	    	  map.put("otherdetails", otherDetails);
	    	
	    	 return "otherDetails";
	    }
	    
	    
	    
	    
	    
	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}


	public AddressService getAddressService() {
		return addressService;
	}


	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}


	public SkillDetailsService getSkillService() {
		return skillService;
	}


	public void setSkillService(SkillDetailsService skillService) {
		this.skillService = skillService;
	}


	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}


	public void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}


	public DocumentService getDocumentService() {
		return documentService;
	}


	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}


	public EducationDetailsService getEducationDetailsService() {
		return educationDetailsService;
	}


	public void setEducationDetailsService(EducationDetailsService educationDetailsService) {
		this.educationDetailsService = educationDetailsService;
	}


	public FamilyDetailsService getFamilyDetailsService() {
		return familyDetailsService;
	}


	public void setFamilyDetailsService(FamilyDetailsService familyDetailsService) {
		this.familyDetailsService = familyDetailsService;
	}


	public EmployementHistoryService getEmploymentHistoryService() {
		return employmentHistoryService;
	}


	public void setEmploymentHistoryService(EmployementHistoryService employmentHistoryService) {
		this.employmentHistoryService = employmentHistoryService;
	}
	 public CertificateDetailsService getCertificateDetailsService() {
			return certificateDetailsService;
		
	 }

	public void setCertificateDetailsService(CertificateDetailsService certificateDetailsService) {
			this.certificateDetailsService = certificateDetailsService;
		
	}

	public EmployeeProjectService getEmployeeProjectService() {
		return employeeProjectService;
	}


	public void setEmployeeProjectService(
			EmployeeProjectService employeeProjectService) {
		this.employeeProjectService = employeeProjectService;
	}
	
	 private File getFile() throws FileNotFoundException {
	        File file = new File(baseDir);
	        if (!file.exists()){
	            throw new FileNotFoundException("file with path: " + baseDir + " was not found.");
	        }
	        return file;
	    }
	 
	 @ResourceMapping(value="deletechildEdu")
	 public void deletechildEdu(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
	 	//List<String> projectId = new ArrayList<String>();
	 	System.out.println("inside *******deletechild");
	 	//travelClaimService.deleteChild(child);
	 	com.hrms.entity.EducationDetails educationdetails=new com.hrms.entity.EducationDetails();
	 	educationdetails.setEduId(new Long(child));
	 	com.hrms.entity.Employee employee=new com.hrms.entity.Employee();
	 	employee.setSid(Long.valueOf(empId));
	 	educationdetails.setEmpId(employee);
	 	educationDetailsService.updateEducationDetails(educationdetails,false);
	 	
	 	
	 }
	 
	 
	 @ResourceMapping(value="deletechildAdd")
	 public void deletechildAdd(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
	 	//List<String> projectId = new ArrayList<String>();
	 	System.out.println("inside *******deletechildAdd");
	 	//travelClaimService.deleteChild(child);
	 	com.hrms.entity.Address addressdetails=new com.hrms.entity.Address();
	 	addressdetails.setId(new Long(child));
	 	//com.hrms.entity.Employee employee=new com.hrms.entity.Employee();
	 	//employee.setSid(Long.valueOf(empId));
	 	//addressdetails.setEmployeeId(employee);
	 	addressService.updateAddressDetails(addressdetails, false);
	 	
	 }
	 @ResourceMapping(value="deletechildCert")
	 public void deletechildCert(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
	 	//List<String> projectId = new ArrayList<String>();
	 	System.out.println("inside *******deletechildCert");
	  	//travelClaimService.deleteChild(child);
	 	com.hrms.entity.CertificationDetails certificationdetails=new com.hrms.entity.CertificationDetails();
	 	certificationdetails.setId(new Long(child));
	 	//com.hrms.entity.Employee employee=new com.hrms.entity.Employee();
	 	//employee.setSid(Long.valueOf(empId));
	 	//addressdetails.setEmployeeId(employee);
	 	certificateDetailsService.updateCertificationDetails(certificationdetails, false);
	 	
	 }
	 
	 @ResourceMapping(value="deletechildFamily")
	 public void deletechildFamily(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
	 	//List<String> projectId = new ArrayList<String>();
	 	System.out.println("inside *******deletechildCert");
	 	//travelClaimService.deleteChild(child);
	 	com.hrms.entity.FamilyDetails familydetails=new com.hrms.entity.FamilyDetails();
	 	familydetails.setSid(new Long(child));
	 	//com.hrms.entity.Employee employee=new com.hrms.entity.Employee();
	 	//employee.setSid(Long.valueOf(empId));
	 	//addressdetails.setEmployeeId(employee);
	 	familyDetailsService.updateFamilyDetails(familydetails, false);
	 	
	 }
	 
	 @ResourceMapping(value="deletechildSkill")
	 public void deletechildSkill(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
	 	//List<String> projectId = new ArrayList<String>();
	 	System.out.println("inside *******deletechildCert");
	 	//travelClaimService.deleteChild(child);
	 	com.hrms.entity.SkillDetails skilldetails=new com.hrms.entity.SkillDetails();
	 	skilldetails.setSlNo(Long.valueOf(child));
	 	//com.hrms.entity.Employee employee=new com.hrms.entity.Employee();
	 	//employee.setSid(Long.valueOf(empId));
	 	//addressdetails.setEmployeeId(employee);
	 	skillService.updateSkillDetail(skilldetails,false);
	 	
	 }
	 
	 @ActionMapping(params={"action=projectDetails"})
	  public void projectDetailsAction(ActionResponse actionResponse , ActionRequest actionRequest,Map<String, Object> map)
	  {
	    if (LOGGER.isTraceEnabled()) {
	      LOGGER.trace("projectDetailsAction");
	    }
	    
	    try {
	    	
			User currentUser = PortalUtil.getUser(actionRequest);
			currentUser.getUserId();
			com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
			List<EmployementHistory> employementHistories=employmentHistoryService.getEmploymentHistoryByCompanyEmployee(""+employeeEntity.getSid());
			
			List<ProjectDetails> projectEntityList=null;
			
			
			EmployeeProjectDetails employmentProjectDetails=new EmployeeProjectDetails();
			for(EmployementHistory  employementHistory:employementHistories)
			{
				
				companyMap.put(""+employementHistory.getId(), employementHistory.getCompany());
			}
			employmentProjectDetails.setCompanyMap(companyMap);
			employmentProjectDetails.setRoleMap(roleMap);
			
			if(null!=employeeEntity)
			{
				projectEntityList=employeeProjectService.getEmployeeProjectByEmployeeId(""+employeeEntity.getSid());

			}
			
			List<com.hrms.model.ProjectDetails> projectList=new ArrayList<com.hrms.model.ProjectDetails>();
			
			  if(null!=projectEntityList && projectEntityList.size()>0)
		        {
		        	for(ProjectDetails projectEntity:projectEntityList)
		        	{
		        		com.hrms.model.ProjectDetails proj=new com.hrms.model.ProjectDetails();
		        		proj.setId(""+projectEntity.getId());
		        		if(null!=projectEntity.getRoleId())
		        		proj.setPrjectRole(""+projectEntity.getRoleId());
		        		proj.setProjectAchievements(projectEntity.getKeyContribution());
		        		proj.setProjectDescription(projectEntity.getProjectDescription());
		        		proj.setProjectEndDate(projectEntity.getEndDate());
		        		proj.setProjectName(projectEntity.getProjectName());
		        		proj.setProjectStartDate(projectEntity.getStartDate());
		        		proj.setEnable(projectEntity.isEnable());
		        		proj.setSuccess(projectEntity.isSuccess());
		        		proj.setClientName(projectEntity.getClientName());
		        		if(null!=projectEntity.getEmploymentHistory())
		        		proj.setCompanyId(""+projectEntity.getEmploymentHistory().getId());
		        		
		        		List<String > roleList = new ArrayList<String>();

		        		if(null!=projectEntity.getRoleId() && !projectEntity.getRoleId().equals(""))
		        		{
		        			String roles[]=projectEntity.getRoleId().split(",");
		        			for(String role1:roles)
		        			{
		        				roleList.add(role1);
		        			}
		        			proj.setRoleList(roleList);
		        		}
		        		
			        	proj.setRole(""+projectEntity.getRoleId());
		        		
			        	projectList.add(proj);
		        	}
		        	
		        	
		        }
			
			
			  employmentProjectDetails.setProjectDetailList(projectList);
			
			map.put("projectDetails", employmentProjectDetails);
	
    
    } catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	     actionResponse.setRenderParameter("render", "projectDetails-view");
	  }
	  
	  @RenderMapping(params={"render=projectDetails-view"})
	  public String projectDetailsView(Map<String, Object> map, RenderRequest actionRequest)
	  {
	    if (LOGGER.isTraceEnabled()) {
	      LOGGER.trace("projectDetailsView");
	    }
	    
		try {
			    	
					User currentUser = PortalUtil.getUser(actionRequest);
					currentUser.getUserId();
					com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
					List<EmployementHistory> employementHistories=employmentHistoryService.getEmploymentHistoryByCompanyEmployee(""+employeeEntity.getSid());
					
					List<ProjectDetails> projectEntityList=null;
					
					
					EmployeeProjectDetails employmentProjectDetails=new EmployeeProjectDetails();
					for(EmployementHistory  employementHistory:employementHistories)
					{
						
						companyMap.put(""+employementHistory.getId(), employementHistory.getCompany());
					}
					employmentProjectDetails.setCompanyMap(companyMap);
					employmentProjectDetails.setRoleMap(roleMap);
					
					if(null!=employeeEntity)
					{
						projectEntityList=employeeProjectService.getEmployeeProjectByEmployeeId(""+employeeEntity.getSid());

					}
					
					List<com.hrms.model.ProjectDetails> projectList=new ArrayList<com.hrms.model.ProjectDetails>();
					
					  if(null!=projectEntityList && projectEntityList.size()>0)
				        {
				        	for(ProjectDetails projectEntity:projectEntityList)
				        	{
				        		com.hrms.model.ProjectDetails proj=new com.hrms.model.ProjectDetails();
				        		proj.setId(""+projectEntity.getId());
				        		if(null!=projectEntity.getRoleId())
				        		proj.setPrjectRole(""+projectEntity.getRoleId());
				        		proj.setProjectAchievements(projectEntity.getKeyContribution());
				        		proj.setProjectDescription(projectEntity.getProjectDescription());
				        		proj.setProjectEndDate(projectEntity.getEndDate());
				        		proj.setProjectName(projectEntity.getProjectName());
				        		proj.setProjectStartDate(projectEntity.getStartDate());
				        		proj.setEnable(projectEntity.isEnable());
				        		proj.setSuccess(projectEntity.isSuccess());
				        		proj.setClientName(projectEntity.getClientName());
				        		if(null!=projectEntity.getEmploymentHistory())
				        		proj.setCompanyId(""+projectEntity.getEmploymentHistory().getId());
				        		
				        		List<String > roleList = new ArrayList<String>();

				        		if(null!=projectEntity.getRoleId() && !projectEntity.getRoleId().equals(""))
				        		{
				        			String roles[]=projectEntity.getRoleId().split(",");
				        			for(String role1:roles)
				        			{
				        				roleList.add(role1);
				        			}
				        			proj.setRoleList(roleList);
				        		}
				        		
					        	proj.setRole(""+projectEntity.getRoleId());
					        	projectList.add(proj);
				        	}
				        	
				        	
				        }
					
					
					  employmentProjectDetails.setProjectDetailList(projectList);
					
					map.put("projectDetails", employmentProjectDetails);
			
		    
		    } catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    
	    return "EmployeeProjectDetails";
	  }


	  @ActionMapping("saveEmploymentProject")
	  public void saveEmploymentProjectDetails(@ModelAttribute("projectDetails")EmployeeProjectDetails projectDetails1,ActionResponse actionResponse, ActionRequest actionRequest)throws Exception
	  {
	     if (LOGGER.isTraceEnabled()) {
	      LOGGER.trace("***************************saveEmploymentProjectDetails*****************************");
	    }
	     User currentUser=null;
		try {
			
				List<ProjectDetails> projectDetailList=new  ArrayList<ProjectDetails>();
				JSONParser parser = new JSONParser();
				currentUser = PortalUtil.getUser(actionRequest);
				currentUser.getUserId();
				com.hrms.entity.Employee employeeEntity=employeeService.getEmployeeByUserId(""+currentUser.getUserId()); 
				//List<EmployementHistory> employementHistory=new ArrayList<EmployementHistory>();
				//EmployementHistory employementHistory2=employementHistory.get(0);
				String inputString = actionRequest.getParameter("projectDetails");
		     
				
				if(actionRequest.getParameter("saveandExitName").equals("saveAndExit"))
				  {
			   		  System.out.println("save and exit");
			   		  JSONArray json = (JSONArray)parser.parse(inputString);
					
					for (int i = 0; i < json.size(); i++)
		  	        {
		  	         
						  JSONObject objects = (JSONObject)json.get(i);
  	      				  ProjectDetails projectDetails=new ProjectDetails();
		  	              projectDetails.setEndDate((String)objects.get("endDate"));
		  	              projectDetails.setStartDate((String)objects.get("startDate"));
		  	              projectDetails.setKeyContribution((String)objects.get("keyContribution"));
		  	              projectDetails.setProjectDescription((String)objects.get("projectDescription"));
		  	              projectDetails.setProjectName((String)objects.get("projectName"));
		  	              projectDetails.setClientName((String)objects.get("clientName"));
		  	              
		  	             /* Role role=new Role();
		  	              if(!((String)objects.get("role")).equals(""))
		  	              role.setId(Long.valueOf((String)objects.get("role")));
		  	              projectDetails.setRoleId((String)objects.get("role"));*/
		  	              String role="";
		  	              if(null!=(JSONArray)objects.get("role"))
		  	              {
		  	            	JSONArray roles=(JSONArray)objects.get("role");
		  	            	for(int j = 0; j < roles.size(); j++)
		  	            	{
		  	            		role=role+(String)roles.get(j)+",";
		  	            	}
		  	              }
		  	              projectDetails.setRoleId(role);
		  	              
		  	              
		  	              EmployementHistory employementHistory=new EmployementHistory();
		  	              if(!((String)objects.get("companyId")).equals(""))
		  	              employementHistory.setId(Long.valueOf((String)objects.get("companyId")));
		  	              projectDetails.setEmploymentHistory(employementHistory);
		  	              
		  	              com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
			  	          emp.setSid(employeeEntity.getSid());
			  	          projectDetails.setEmpId(emp);
			  	          
			  	          if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
			  	          {
			  	        	projectDetails.setId(Long.valueOf((String)objects.get("slNo")));

			  	          }
		  	              
			  	          projectDetails.setEnable(true);
		  	              projectDetailList.add(projectDetails);
		  	              
		  	        }
	  	            employeeProjectService.saveEmployeeProject(projectDetailList);
	  	            actionResponse.setRenderParameter("render", "saveExitSuccess-view"); 

				  }
				else
				{
					JSONArray json = (JSONArray)parser.parse(inputString);
					
					for (int i = 0; i < json.size(); i++)
		  	        {
		  	         
						  JSONObject objects = (JSONObject)json.get(i);
  	      				  ProjectDetails projectDetails=new ProjectDetails();
		  	              projectDetails.setEndDate((String)objects.get("endDate"));
		  	              projectDetails.setStartDate((String)objects.get("startDate"));
		  	              projectDetails.setKeyContribution((String)objects.get("keyContribution"));
		  	              projectDetails.setProjectDescription((String)objects.get("projectDescription"));
		  	              projectDetails.setProjectName((String)objects.get("projectName"));
		  	              projectDetails.setClientName((String)objects.get("clientName"));
		  	              
		  	            /*  Role role=new Role();
		  	              if(!((String)objects.get("role")).equals(""))
		  	              role.setId(Long.valueOf((String)objects.get("role")));
		  	              projectDetails.setRoleId((String)objects.get("role"));*/
		  	             
		  	              
		  	            String role="";
		  	              if(null!=(JSONArray)objects.get("role"))
		  	              {
		  	            	JSONArray roles=(JSONArray)objects.get("role");
		  	            	for(int j = 0; j < roles.size(); j++)
		  	            	{
		  	            		role=role+(String)roles.get(j)+",";
		  	            	}
		  	              }
		  	              projectDetails.setRoleId(role);
		  	              
		  	              
		  	              EmployementHistory employementHistory=new EmployementHistory();
		  	              if(!((String)objects.get("companyId")).equals(""))
		  	              employementHistory.setId(Long.valueOf((String)objects.get("companyId")));
		  	              projectDetails.setEmploymentHistory(employementHistory);
		  	              
		  	              com.hrms.entity.Employee emp=new com.hrms.entity.Employee();
			  	          emp.setSid(employeeEntity.getSid());
			  	          projectDetails.setEmpId(emp);
			  	          projectDetails.setEnable(true);
			  	          if(null!=(String)objects.get("slNo") && !((String)objects.get("slNo")).equals("on"))
			  	          {
			  	        	projectDetails.setId(Long.valueOf((String)objects.get("slNo")));

			  	          }
		  	              projectDetailList.add(projectDetails);
		  	              
		  	              
		  	        }
	  	            employeeProjectService.saveEmployeeProject(projectDetailList);
	  	          actionResponse.setRenderParameter("render", "certificationtrainingDetails-view"); 
				}
				
		     
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	     
	     
	  }
	  	@ResourceMapping(value="deletechildProject")
		 public void deletechildProject(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
		 	System.out.println("inside *******deletechildProject");
		 	com.hrms.entity.ProjectDetails projectDetails=new com.hrms.entity.ProjectDetails();
		 	projectDetails.setId(new Long(child));
		 	employeeProjectService.updateEmployeeProject(projectDetails, false);
		 	
		 }
	  	@ResourceMapping(value="deletechildEmployment")
		 public void deletechildEmployment(@RequestParam(value="child",required=true)String child,ResourceRequest request, ResourceResponse response,Map<String, Object> map){
		 	System.out.println("inside *******deletechildProject");
		 	com.hrms.entity.EmployementHistory employementhistory=new com.hrms.entity.EmployementHistory();
		 	employementhistory.setId(new Long(child));
		 	employmentHistoryService.updateEmployementHistory(employementhistory,false);
		 	
		 }
		 
			@ResourceMapping(value = "downloadAadharFile")	
        public void downloadAadharFileDoc(ResourceRequest request,ResourceResponse response, Map<String, Object> map) throws IOException, SQLException 
		{
			if (LOGGER.isTraceEnabled()) 
			{
			  LOGGER.trace("familyDetailsAdharDocumentDownload");
			}
			
			PortletSession session = request.getPortletSession();
			com.hrms.entity.Employee employeeEntity = ( com.hrms.entity.Employee)session.getAttribute("employe",PortletSession.APPLICATION_SCOPE);
			  
			UploadPortletRequest downloadRequest = PortalUtil.getUploadPortletRequest(request);
			
			  String downloadNumber = downloadRequest.getParameter("downloadNumber");
			
			  System.out.println(" downloadFileURL downloadnum:"+downloadNumber);
              
              List<com.hrms.entity.FamilyDetails> familyDetails=null;
			if(null!=employeeEntity)
			{
				familyDetails=familyDetailsService.getFamilyDetailsByEmpSid(employeeEntity.getSid());
				
				Blob adharDocumentBlob=null;
				String adharFilename="";
				String adharNo="";
		        if(null!=familyDetails && familyDetails.size()>0)
		        {
		        	for(com.hrms.entity.FamilyDetails familyDetails2:familyDetails)
		        	{
						adharDocumentBlob = familyDetails2.getAdhaarDocument();
						adharFilename = familyDetails2.getAdharFileName();
						adharNo = familyDetails2.getAadharNo();
						
						if(adharDocumentBlob!= null && adharNo.equals(downloadNumber)) 
						{
							byte[] fileBytes = null;
							try {
									fileBytes = adharDocumentBlob.getBytes(1, (int)adharDocumentBlob.length());
							  
								}
								  catch (Exception e) 
								  { 
										// TODO Auto-generated catch block
										e.printStackTrace();
								  }
							  response.setContentType("application/octet-stream");
							  response.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
							  response.addProperty(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename="+ adharFilename);
							  
							  //System.out.println("fileBytes length:"+fileBytes.length);
							  OutputStream out = response.getPortletOutputStream();
							  InputStream in = new ByteArrayInputStream(fileBytes);

							  byte[] buffer = new byte[4096];
							  int len;

							  while ((len = in.read(buffer)) != -1) {
									out.write(buffer, 0, len);
							  }
							  
								out.flush();
								in.close();
								out.close();
							}
						}

					}
				
				}
              
			}
}
